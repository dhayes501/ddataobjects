<?php
// the top 3 lines of this file are removed when creating it via the program and are
// replaced with the lines necessary to turn it into a class.  look in setup.php

	// display the last error message stored in the $this->_last_error variable
	public function last_error($return_value = 0)
	{
		if ($return_value)
			return $this->_last_error;
		echo $this->_last_error;
		return 1;
	}

	// returns the values of the class that are DB fields.  return value
	public function debug($return_value = 0, $print_on_screen = 0)
	{
		if ($print_on_screen) $string = "TABLE NAME: $this->_prepend_dot$this->_name\n"; else $string = "<table border=1><tr><td><b>TABLE NAME:</b></td><td>$this->_prepend_dot$this->_name</td></tr>\n";
		foreach($this->_columns as $column_name=>$a)
		{
			if ($print_on_screen) $string .= "$column_name: "; else $string .= "<tr><td><b>$column_name:</b></td><td>";
			if (is_array($this->{$column_name}))
			{
				if ($print_on_screen) $string .= "Array:\n"; else $string .= "Array<br>\n";
				foreach($this->{$column_name} as $k=>$v)
				{
					if (is_array($k))
					{
						if ($print_on_screen) $string .= " - Array:\n"; else $string .= " - Array<br>\n";
						foreach($k as $k2=>$v2)
						{
							if ($print_on_screen) $string .= " - $k2: $v2\n"; else $string .= " - $k2: $v2<br>\n";
						}
					}
					else
					{
						if ($print_on_screen) $string .= "$k: $v\n"; else $string .= "$k: $v<br>\n";
					}
				}
				if ($print_on_screen) $string .= ""; else $string .= "</td></tr>\n";
			}
			else
			{
				$temp_value = $this->{$column_name};
				if ($print_on_screen) $string .= $temp_value."\n";
                    else $string .= $temp_value."&nbsp;</td></tr>\n";
			}
		}

		if ($print_on_screen) $string .= ""; else $string .= "</table>\n";
		if ($return_value)
			return $string;
		echo $string;
		return 1;
	}
	// quickly print a form to add or edit a record
	public function quick_form($value = 'CURRENT', $separator = ': ', $divider = "<br>\n", $notes = '')
	{
		return $this->quick_function(0, $value, $separator, $divider, $notes);
	}
	// quickly display a record with hidden values to confirm an add or edit a record
	public function quick_view($value = 'CURRENT', $separator = ': ', $divider = "<br>\n", $notes = '')
	{
		return $this->quick_function(1, $value, $separator, $divider, $notes);
	}
    public function quick_function($display, $value, $separator, $divider, $notes)
    {
        $value = strtoupper($value);
        $string = '';
        foreach($this->_columns as $column_name=>$a)
        {
            // if this is the table's primary key, skip it
            if ($this->_primary_key_column == $column_name || $this->_columns[$column_name]['field_type'] == 'none')
                continue;

            if (is_array($this->{$column_name}))
            {
                // this isn't supported yet
            }
            else
            {
                if (!$input_type = $this->_columns[$column_name]['input_type'])
                {
                    if ($this->_columns[$column_name]['length'] > 100)
                        $input_type = 'textarea';
                    else
                        $input_type = 'textbox';
                }
                $default_value = $this->_columns[$column_name]['default_value'];
                if (!$size_rows_elements = $this->_columns[$column_name]['size_rows_elements'])
                {
                    if ($input_type == 'textbox') $size_rows_elements = $this->_columns[$column_name]['length'];
                    if ($input_type == 'textarea') $size_rows_elements = '5';
                }
                elseif ($input_type == 'select' || $input_type == 'radio')
                { // if there are values and its a select or radio, we need to parse the string to set the array
                    $temp_array = explode("&", $size_rows_elements);
                    $temp_string = '';
                    foreach($temp_array as $temp_line)
                    {
                        list($temp_key, $temp_value) = explode("=", $temp_line);
                        $temp_string .= urldecode($temp_key)."|".urldecode($temp_value)."||";
                    }
                    $temp_string = substr($temp_string, 0, count($temp_string)-2);
                    $size_rows_elements = $temp_string;
                }
                if (!$columns_spacer_multiple = $this->_columns[$column_name]['columns_spacer_multiple'])
                {
                    if ($input_type == 'textarea') $columns_spacer_multiple = '50';
                    if ($input_type == 'radio') $columns_spacer_multiple = ' &nbsp; ';
                }
                $additional_params = $this->_columns[$column_name]['additional_params'];
                $string .= $this->{"$column_name"."_label"}.$separator;
                $string .= $this->{"get_".$column_name."_ie"}($input_type, $default_value, $size_rows_elements, $columns_spacer_multiple, $display, $additional_params, $value);
            }
            if ($notes)
            {
                $string .= " &nbsp; ";
                if ($notes == 'REGULAR_COMMENTS') $string .= $this->{$column_name."_regular_comments"};
                    else $string .= $notes;
            }
            $string .= $divider;
        }
        return $string;
    }
	public function get_variable($column_name = null)
	{
		if (!isset($this->{$column_name}))
		{
			$this->_last_error = "$column_name: ".$this->{$column_name}." --was not set";
			return false;
		}
		return $this->{$column_name};
	}
	public function toggle($column_name = null)
	{
		if (!isset($this->{$column_name}) || $this->{$column_name} == 'FALSE')
			$this->{$column_name} = 'TRUE';
		else
			$this->{$column_name} = 'FALSE';
	}

	// sets the class variable "field_name" equal to "value".  does any validation and sets any "_display" variables
	public function set_variable($field_name, $value)
	{
		// get field meta_data
		$md = $this->_columns[$field_name];
		// Is null Allowed
		if (@$md['type'] != 'boolean')
		{
			// if the column cannot be null and doesn't have a default value AND it isn't given a value, return false
			if (@$md['notnull'] == 't' && @$md['default'] == '')
			{
				if (is_string($value) && strlen(trim($value)) == 0)
				{
					$this->_last_error = "$field_name cannot be set to \"$value\" which is nothing";
					return false;
				}
			}
			// if the value is not required and not set, set to null
			if (is_string($value) && strlen(trim($value)) == 0)
			{
				unset($this->{$md['name']});
				return true;
			}
			// if the column has a length maximum, make sure it isn't reached
			if (@$md['length'] > 0)
			{
				if (is_string($value) && strlen(trim($value)) > @$md['length'])
				{
					$this->_last_error = "$field_name has a maximum column length of $md[length]. The ".strlen(trim($value))." length content is too long:<br>\n".trim($value);
					return false;
				}
			}
		}
		if (@$md['type'] == 'integer' || @$md['type'] == 'bigint')
		{
			// return false if it isn't an integer
			if (!is_int_val($value))
			{
				$this->_last_error = "$field_name is an integer and cannot be set to: $value";
				return false;
			}
		}
		if (isset($md['regexp']) && is_string($md['regexp']) && strlen(trim($md['regexp'])) > 0)
        {
			#$regexp = base64_decode($md['regexp']);
			$regexp = $md['regexp'];
			// if they've given a regular expression to use to check the value, make sure it passes
                       	if (!@preg_match("/$regexp/", $value))
			{
				$this->_last_error = "$field_name cannot be set to:<br>\n$value<br>\nBecause it doesn't satisfy the regular expression:<br>\n/$regexp/";
				return false;
			}
        }
		if (@$md['type'] == 'boolean')
		{
			// allow them to enter any form of true or false to set the boolean
			$value = strtoupper($value);
	        if ($value == 'T' || $value == '1' || $value === true)
			{
				$value = 'TRUE';
				$this->{$md['name']."_b"} = TRUE;
			}
	        if ($value == 'F' || $value == '0' || $value == '' || $value === false)
			{
				$value = 'FALSE';
				$this->{$md['name']."_b"} = FALSE;
			}
	        if ($value != 'TRUE' && $value != 'FALSE')
			{
				$this->_last_error = "$field_name is a boolean value.  \"$value\" is not.";
				return false;
			}
		}
		if (@$md['type'] == 'date')
		{

			if (!$date_array = split_date($value))
			{
				$this->_last_error = "$field_name is a date field.  Cannot parse date: $value";
				return false;
			}
			if (strlen($date_array['year']) == 4)
		        	$value = "$date_array[year]-$date_array[month]-$date_array[day]";
			else
		        	$value = "$date_array[month]-$date_array[day]-$date_array[year]";
	        	$this->{$md['name']."_display"} = "$date_array[month]/$date_array[day]/$date_array[year]";
		}
		if (@$md['type'] == 'double precision')
		{
                        $negative = false;
                        if (substr($value, 0, 1) == '-')
                                $negative = true;
                        // remove non-digits
                        $value = preg_replace("/[^0-9\.]/", "", $value);
                        if (!is_double_val($value))
			{
				$this->_last_error = "$field_name is a double precision field.  \"$value\" is not";
                                return false;
			}
                        if ($negative)
                                $value = "-$value";
		}
		if (@$md['type'] == 'timestamp without time zone')
		{
			if (!$ts_array = is_timestamp_val($value))
			{
				$this->_last_error = "$field_name is a timestamp field.  Cannot parse timestamp: $value";
				return false;
			}
			if ($ts_array['hour'] > 12)
			{
				$h=$ts_array['hour']-12;
				$suffix='pm';
			}
			elseif($ts_array['hour'] == 12)
			{
				$h=12;
				$suffix='pm';
			}
			else
			{
				$h=$ts_array['hour'];
				$suffix='am';
			}
        		$this->{$md['name']."_display"} = "$ts_array[month]/$ts_array[day]/$ts_array[year] $h:$ts_array[minute]$suffix";
			$value = "$ts_array[year]/$ts_array[month]/$ts_array[day] $ts_array[hour]:$ts_array[minute]:$ts_array[second]";
			if (@strlen($ts_array['milliseconds']) > 0) $value .= ".$ts_array[milliseconds]";
		}
		if (@$md['type'] == 'time without time zone')
		{
			if (!$t_array = is_time_val($value))
			{
				$this->_last_error = "$field_name is a time field.  Cannot parse time: $value";
				return false;
			}
			if ($t_array['hour'] > 12)
			{
				$h=$t_array['hour']-12;
				$suffix='pm';
			}
			elseif($t_array['hour'] == 12)
			{
				$h=12;
				$suffix='pm';
			}
			else
			{
				$h=$t_array['hour'];
				$suffix='am';
			}
			$this->{$md['name']."_display"} = "$h:$t_array[minute]$suffix";
			$value = "$t_array[hour]:$t_array[minute]:$t_array[second]";
		}

		if (@$md['field_type'] == 'phone_number')
		{
			// remove non-digits
			$value = preg_replace("/[^0-9]/", "", $value);
			// make sure there's 10 digits
			if (strlen($value) != 10)
			{
				$this->_last_error = "$field_name is a phone_number text field.  \"$value\" (after stripping non-digits) is not a 10 number string";
				return false;
			}
			// format as phone number
			$temp = $value;
			$value = '('.substr($temp, 0, 3).') '.substr($temp, 3, 3).'-'.substr($temp, 6, 4);
		}
		if (@$md['field_type'] == 'zip_code')
		{
			// remove non-digits
			$value = preg_replace("/[^0-9]/", "", $value);
			// make sure there's 5 or 9 digits
			if (strlen($value) != 5 && strlen($value) != 9)
			{
				$this->_last_error = "$field_name is a zip_code text field.  \"$value\" (after stripping non-digits) is not a 5 or 9 number string";
				return false;
			}
			// format as zip code
			$temp = substr($value, 0, 5);
			if ($temp != $value)
				$temp .= "-".substr($value, 5, 4);
			$value = $temp;
		}
		if (@$md['field_type'] == 'email')
		{
			$email_regexp = "/^[_a-zA-Z0-9+-]+(\.[_a-zA-Z0-9+-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/";
                       	if (!preg_match($email_regexp, $value))
			{
				$this->_last_error = "$field_name is an email text field.  \"$value\" does not conform to specified regexp:<br>\n$email_regexp";
				return false;
			}
		}
		if (@$md['field_type'] == 'currency')
		{
			// currency should be a double precision field.  confirm this
			$negative = false;
                        if (substr($value, 0, 1) == '-')
                                $negative = true;
                        // remove non-digits
                        $value = preg_replace("/[^0-9\.]/", "", $value);
                        if (!is_double_val($value))
			{
				$this->_last_error = "$field_name is a \"currency\" double precision field.  \"$value\" is not";
                                return false;
			}
                        if ($negative)
                                $value = "-$value";

			// format as currency
                       	$this->{$md['column_name']."_display"} = number_format($value, 2, '.', ',');
		}
		if (@$md['field_type'] == 'ssn')
		{
			// remove non-digits
			$value = preg_replace("/[^0-9]/", "", $value);
			// make sure there's 9 digits
			if (strlen($value) != 9)
			{
				$this->_last_error = "$field_name is a ssn text field.  \"$value\" (after stripping non-digits) is not a 9 number string";
				return false;
			}
			// format as ssn
			$temp1 = substr($value, 0, 3);
			$temp2 = substr($value, 3, 2);
			$temp3 = substr($value, 5, 4);
			$value = "$temp1-$temp2-$temp3";
		}
		if (@$md['field_type'] == 'mmdd')
		{
			// remove non-digits
			$value = preg_replace("/[^0-9]/", "", $value);
			// make sure there's 4 digits
			if (strlen($value) != 4)
			{
				$this->_last_error = "$field_name is a mmdd text field.  \"$value\" (after stripping non-digits) is not a 4 number string";
				return false;
			}
			// format as mmdd
			$mm = substr($value, 0, 1);
			$dd = substr($value, 2, 3);
			$value = "$mm/$dd";
			if (!checkdate($mm,$dd,"9999"))
			{
				$this->_last_error = "$field_name is a mmdd text field.  \"$mm/$dd/9999\" is not a valid date";
				return false;
			}
		}
		$this->{$md['name']} = $value;
		return true;
	}
	public function get_lookup_values($name = null)
	{
		if (!$name)
		{
			$this->_last_error = "\"$name\" is not set. You must specify a field";
			return false;
		}
		if (!isset($this->{$name}))
		{
			$this->_last_error = "\"$name\" is not a field.";
			return false;
		}
		return $this->{$name};
	}
  	public function set_lookup_values($name = null, $values = null)
	{
		if (!$name)
		{
			$this->_last_error = "\"$name\" is not set. You must specify a field";
			return false;
		}
		if (gettype($values) == 'NULL')
		{
			$this->{$name} = null;
			return true;
		}
		$this->{$name} = array();
		if (@is_array($values))
			foreach($values as $new_value)
				$this->{$name}[] = $new_value;
		return true;
	}
	// get html input element for lookup value
	public function get_lookup_values_ie($name = null, $type, $value, $size_rows_elements, $columns_spacer_multiple, $display, $additional_params, $use_default_value, $alt_css_id)
	{
		if (!$name)
		{
			$this->_last_error = "\"$name\" is not set. You must specify a field";
			return false;
		}

		$md = $this->{"_".$name."_meta"};

		if ($use_default_value == 'GET')
			$value = @$_GET[$this->_name][$name];
		if ($use_default_value == 'POST')
			$value = @$_POST[$this->_name][$name];
		if ($use_default_value == 'CURRENT')
			$value = @$this->{$name};

		$a['table_name'] = $this->_name;
		$a['column_name'] = $name;
		if (@$md['length'] > 0)
			$a['column_length'] = $md['length'];
		$a['type'] = $type;

		$a['value'] = $value;
		$a['size'] = $size_rows_elements;
		$a['rows'] = $size_rows_elements;
		$a['elements'] = $size_rows_elements;
		$a['columns'] = $columns_spacer_multiple;
		$a['spacer'] = $columns_spacer_multiple;
		$a['multiple'] = $columns_spacer_multiple;
		$a['display'] = $display;
		$a['additional_params'] = $additional_params;
		$a['id'] = $alt_css_id;
		return get_form_element($a);
	}
	public function get_variable_ie($column_name = null, $type, $value, $size_rows_elements, $columns_spacer_multiple, $display, $additional_params, $use_default_value, $alt_css_id)
	{
		if (!$column_name)
		{
			$this->_last_error = "\"$column_name\" is not set";
			return false;
		}

		$md = $this->_columns[$column_name];

		if ($use_default_value == 'GET')
			$value = @$_GET[$this->_name][$column_name];
		if ($use_default_value == 'POST')
			$value = @$_POST[$this->_name][$column_name];
		if ($use_default_value == 'CURRENT')
		{
			$md = $this->_columns[$field_name];
			if (@$md['type'] == 'date' || $md['type'] == 'timestamp without time zone' || $md['type'] == 'time without time zone')
			{
				$value = @$this->{$column_name."_display"};
			}
			else
			{
				$value = @$this->{$column_name};
			}
		}

		$a['table_name'] = $this->_name;
		$a['column_name'] = $column_name;
		if (@$md['length'] > 0)
			$a['column_length'] = $md['length'];
		$a['type'] = $type;

		$a['value'] = $value;
		$a['size'] = $size_rows_elements;
		$a['rows'] = $size_rows_elements;
		$a['elements'] = $size_rows_elements;
		$a['columns'] = $columns_spacer_multiple;
		$a['spacer'] = $columns_spacer_multiple;
		$a['multiple'] = $columns_spacer_multiple;
		$a['display'] = $display;
		$a['additional_params'] = $additional_params;
		$a['id'] = $alt_css_id;
		return get_form_element($a);
	}
	// get value to load in an input element
	public function get_form_variable($column_name = null)
	{
		if (!$column_name)
		{
			$this->_last_error = "\"$column_name\" is not set";
			return false;
		}
		if (!isset($this->{$column_name}))
		{
			$this->_last_error = "\"$column_name\" is not a field.";
			return false;
		}
		return formmod($this->{$column_name});
	}
	// get value to load in an input element
	public function get_form_variable_display($column_name = null)
	{
		if (!$column_name)
		{
			$this->_last_error = "\"$column_name\" is not set";
			return false;
		}
		if (!isset($this->{$column_name."_display"}))
		{
			$this->_last_error = "\"$column_name\" is not a field.";
			return false;
		}
		return formmod($this->{$column_name."_display"});
	}




	// sets the variables of the table from an array with the indexes of the variables being the values that are set.
	// ADDITIONAL VARIABLES WILL BE CREATED IF ADDITIONAL KEYS ARE ADDED TO THE ARRAY
	public function set_from_array_unsafe($array = '')
	{
		if (@!is_array($array))
		{
			$this->_last_error = "Invalid Array:<br>\n".print_r($array, true);
			return false;
		}
		foreach($array as $k=>$v)
		{
			if (@method_exists($this, "set_$k"))
			{
				if (@!$this->{"set_$k"}($v))
				{
					$this->_last_error = "Failed to set field \"$k\" to value: $v";
					return false;
				}
			}
			else
				$this->{$k} = $v;
		}
		return true;
	}
	// sets the variables of the table from an array with the indexes of the variables being the values that are set.
	public function set_from_array($array = '')
	{
		$errors = null;
		if (@!is_array($array))
		{
			$this->_last_error = "Invalid Array:<br>\n".print_r($array, true);
			return false;
		}
		foreach($array as $k=>$v)
		{
			if (@method_exists($this, "set_$k"))
			{
				if (@!$this->{"set_$k"}($v))
					$errors[] = $this->{$k."_label"};
			}
			else
			{
				$this->_last_error = "Method does not exist: \$this->set_$k()";
				return false;
			}
		}
		return $errors;
	}
	// sets the variables of the table from an array with the indexes of the variables being the values that are set. works with forms so if no value is set, it will default to FALSE for checkboxes
	public function set_from_form($array = '')
	{
		$errors = null;
		if (@!is_array($array))
		{
			$this->_last_error = "Invalid Array:<br>\n".print_r($array, true);
			return false;
		}
		foreach($this->_columns as $column=>$column_info)
		{
			if ($column == $this->_primary_key_column)
				continue;
			if (@!$this->{"set_$column"}($array[$column]))
				$errors[] = $this->{$column."_label"};
		}

		return $errors;
	}

	// sets the variables of the table to an array indexed by variable names
	public function set_to_array()
	{
		$array = array();
		foreach($this->_columns as $name=>$v)
		{
			if ($v['type'] == 'date' || $v['type'] == 'timestamp without time zone' || $v['type'] == 'time without time zone')
				@$array[$name] = $this->{$name."_display"};
			else
				@$array[$name] = $this->{$name};
		}
		return $array;
	}
	// sets all values associated with this class from the record in the database corresponding to the primary key of the table.
	// this public function is only available for tables with 1 or 2 primary keys
	public function get($key1 = false, $key2 = false)
	{
		if ($this->_primary_key)
		{
			$pk = $this->_primary_key_column;
			if (!$key1 && !$this->{$pk})
			{
				$this->_last_error = "\"$key1\" was not specified and a primary key \"".$this->{$pk}."\" does not exist";
				return false;
			}
			if (!$key1)
			{
				$key1 = $this->{$pk};
			}
			else
			{
				if (!$this->set_variable($pk, $key1))
				{
					$this->_last_error = "Unable to set primary key \"$pk\" to \"$key1\"";
					return false;
				}
			}
		        if(@!isset($this->{$pk}))
			{
				$this->_last_error = "Primary key is not set, can't get record";
				return false;
			}

        		$sql = "SELECT * FROM $this->_prepend_dot_q\"$this->_name\" WHERE $pk = '".pg_escape_string($this->{$pk})."';";
		}
		elseif ($this->_primary_keys)
		{
			$pk1 = $this->_primary_key_columns[0];
			$pk2 = $this->_primary_key_columns[1];
			if (!$key1)
				$key1 = $this->{$pk1};
			else
			{
				if (!$this->set_variable($pk1, $key1))
				{
					$this->_last_error = "Unable to set primary key1 \"$pk1\" to \"$key1\"";
					return false;
				}
			}
			if (!$key2)
				$key2 = $this->{$pk2};
			else
			{
				if (!$this->set_variable($pk2, $key2))
				{
					$this->_last_error = "Unable to set primary key2 \"$pk2\" to \"$key2\"";
					return false;
				}
			}
			if (@!isset($this->{$pk1}))
			{
				$this->_last_error = "Primary key 1 is not set, can't get record";
				return false;
			}
			if (@!isset($this->{$pk2}))
			{
				$this->_last_error = "Primary key 2 is not set, can't get record";
				return false;
			}
        		$sql = "SELECT * FROM $this->_prepend_dot_q\"$this->_name\" WHERE $pk1 = '".pg_escape_string($this->{$pk1})."' AND $pk2 = '".pg_escape_string($this->{$pk2})."';";
		}
		else
		{
			$this->_last_error = "No primary keys exist.  Can't get record";
			return false;
		}

		$result = pg_query($this->_dbh, $sql);
	        if (pg_num_rows($result) != '1')
		{
			if ($this->_primary_key) unset($this->{$pk});
			elseif($this->_primary_keys) { unset($this->{$pk1}); unset($this->{$pk2});}
			$this->_last_error = "Query did not return exactly one row.  Query returned \"".pg_num_rows($result)."\" rows";
			return false;
		}
	        $row = pg_fetch_array($result);

		foreach($this->_columns as $name=>$v)
			if (!pg_field_is_null($result, 0, $name))  $this->set_variable($name, $row[$name]);

		$this->get_lookups();

		return true;
	}
	// sets lookup variables for class
	public function get_lookups()
	{
		$schema = '';
                if (strlen($this->_schema) > 0 && $this->_schema != 'public')
                        $schema = "$this->_schema.";
		if ($this->_primary_key)
			$pk = $this->_primary_key_column;
	}

	// called by get_all_sql() and get_find_sql() - does the dirty work of searching linked tables for query strings
	public function simple_search_sql($search_string = null)
	{
		if ($search_string)
		{
			$sql = " WHERE ";

			$search_string = str_replace(", ", " ", $search_string);
			$strings = explode(" ", $search_string);
			foreach($strings as $string)
			{
				$sql .= " ( ";

				foreach($this->_columns as $name=>$v)
				{
					if ($name == 'created' || $name == 'password' || in_array($name, $this->_primary_key_columns) || $name == $this->_primary_key_column || $v['type'] == 'boolean')
						continue;
					else
					{
						if ($v['type'] == 'integer' && !is_int($string))
							continue;
						$sql .= " \"$v[name]\" ILIKE '%".pg_escape_string($string)."%' OR";
					}
				}
				$sql = substr($sql, 0, strlen($sql)-3);
				$sql .= " ) AND";
			}
			$sql = substr($sql, 0, strlen($sql)-4);
		}
		return $sql;
	}
	// called by get_records()
	public function get_all_sql($search_string = null, $added_query = '', $added_query_type = 'AND')
	{
	        $sql = "SELECT * FROM $this->_prepend_dot_q\"$this->_name\"";

		if ($search_string) $sql .= $this->simple_search_sql($search_string);

        	if (!strstr($sql, 'WHERE') && strlen($added_query) > 0) $sql .= ' WHERE '; elseif(strlen($added_query) > 0) $sql .= " $added_query_type ";
	        if (strlen($added_query) > 0) $sql .= " $added_query ";
		return $sql;
	}
	// called by get_records()
	public function get_find_sql($search_string = null, $added_query = '', $added_query_type = 'AND', $called_from = 'find')
	{
		$sql = '';
                if ($search_string || strlen($added_query) > 0)
                {
                        $sql = "( SELECT * FROM $this->_prepend_dot_q\"$this->_name\"";

                        if ($search_string) $sql .= $this->simple_search_sql($search_string);

                        if (!strstr($sql, 'WHERE') && strlen($added_query) > 0) $sql .= ' WHERE '; elseif(strlen($added_query) > 0) $sql .= " $added_query_type ";
                        if (strlen($added_query) > 0) $sql .= " $added_query ";

                        if (strtoupper($added_query_type) == 'AND') $sql .= ") INTERSECT ("; else $sql .= ") UNION (";
                }

                $sql .= "SELECT * FROM $this->_prepend_dot_q\"$this->_name\" WHERE ";

                $found = 0;
                foreach($this->_columns as $name=>$v)
                {
                        if (isset($this->{$name}))
                        {
                                $found = 1;
                                if ($v['type'] == 'boolean')
                                        $sql .= "\"$name\" IS ".$this->{$name}." AND ";
                                else
                                        $sql .= "\"$name\" = '".pg_escape_string($this->{$name})."' AND ";
                        }
                }
                if ($found) $sql = substr($sql, 0, strlen($sql)-4);
                else $sql = substr($sql, 0, strlen($sql)-6);
                if ($search_string || strlen($added_query) > 0)
                        $sql .= ")";
                return $sql;

	}
	// called by get_all_array(), get_all_class(), and find()
  	public function get_records($sort_key = null, $offset = "0", $limit = "10000", $direction = "ASC", $search_string = null, $added_query = '', $added_query_type = 'AND', $called_from = 'get_all_class')
	{
		$new_array = array();

	        if (!$sort_key)
		{
			if ($this->_primary_key) $sort_key = $this->_primary_key_column;
			elseif($this->_primary_keys) $sort_key = $this->_primary_key_columns[0].", ".$this->_primary_key_columns[1];
			else
			{
				foreach($this->_columns as $name=>$v)
				{
					$sort_key = $name;
					break;
				}
			}
		}
		if (!$offset = (int) $offset)
			$offset = "0";
		if (!$limit = (int) $limit)
			$limit = "10000";
	        if ($direction != 'ASC' && $direction != 'DESC')
	                $direction = "ASC";
		// if sort_key contains the sort direction, don't add direction again
		if (substr(strtoupper($sort_key), strlen($sort_key)-4, 4) == 'DESC')
			$direction = '';
		if (substr(strtoupper($sort_key), strlen($sort_key)-3, 3) == 'ASC')
			$direction = '';

	        if (strtoupper($added_query_type) != 'AND' || strtoupper($added_query_type) != 'OR')
	                $added_query_type = "AND";

		if ($called_from == 'get_all_array' || $called_from == 'get_all_class' || $called_from == 'step_array' || $called_from == 'step_class')
			$sql = $this->get_all_sql($search_string, $added_query, $added_query_type);
		elseif ($called_from == 'find' || $called_from == 'get_one' || $called_from == 'step_find' || $called_from == 'count_find')
			$sql = $this->get_find_sql($search_string, $added_query, $added_query_type, $called_from);

		if ($called_from == 'count_find')
		{
			$sql = "SELECT count(*) FROM ($sql) as t";
			$result = pg_query($this->_dbh, $sql);
			$records = pg_fetch_row($result);
			$count = $records[0];
			return $count;

		}

		if ($sort_key) $sql .= " ORDER BY $sort_key $direction";
		$sql .= " LIMIT $limit OFFSET $offset;";
	        $result = pg_query($this->_dbh, $sql);
		if ($called_from == 'step_array' || $called_from == 'step_class' || $called_from == 'step_find')
		{
			if (!$result)
			{
				$this->_last_error = "Failed to execute query.<br>\n$sql<br>\nMessage from Postgresql: ".pg_last_error($this->_dbh);
				return false;
			}
			$this->_step_resource = $result;
			return true;
		}
		if ($called_from == 'find' and pg_num_rows($result) == '0') // find returns false if no records are found
		{
			$this->_last_error = "find public function returned 0 results";
			return false;
		}
	        if (@$records = pg_fetch_all($result))
		{
			if ($called_from == 'get_one')
			{
				if (count($records) != 1)
				{
					$this->_last_error = "get_one public function requires exactly one record to be found, but \"".count($records)."\" records were found";
					return false;
				}
				foreach($this->_columns as $name=>$v)
					if (!pg_field_is_null($result, 0, $name))  $this->set_variable($name, $records[0][$name]);
				if ($this->_primary_key_column)
					$this->get_lookups();
				return true;
			}
			else
			{
				for ($i = 0; $i < count($records); $i++)
				{
					$table_name = "$this->_prepend$this->_name";
					if (strlen($this->_namespace) > 0)
						$table_name = "$this->_namespace\\$table_name";
                                        $c = new $table_name;
					foreach($this->_columns as $name=>$v)
						if (!pg_field_is_null($result, $i, $name))  $c->set_variable($name, $records[$i][$name]);
					if ($this->_primary_key_column)
						$c->get_lookups();

					if ($called_from == 'get_all_array')
						$new_array[] = $c->set_to_array();
					if ($called_from == 'get_all_class' || $called_from == 'find')
						$new_array[] = $c;
				}
			}
		}
	        return $new_array;

	}
        // Queries the database for all records to create an array of elements to pass into a get_field_ie() or _lookup_ie() function
        public function get_all_form($select_fields = null, $sort_key = null)
        {
                if (!$this->_primary_key || !$select_fields || !$sort_key)
		{
			$this->_last_error = "get_all_form requires a primary key, selected_fields, and a sort_key.  one of these things is missing";
                        return false;
		}
                $fields = explode(",", $select_fields);
                $list = array();
                $sql = "SELECT $this->_primary_key_column, ".pg_escape_string($select_fields)." FROM $this->_prepend_dot_q\"$this->_name\" ORDER BY ".pg_escape_string($sort_key);
                $result = pg_query($this->_dbh, $sql);
                if (@$records = pg_fetch_all($result))
                {
                        for($i = 0; $i < count($records); $i++)
                        {
                                $label = '';
                                foreach($fields as $field)
                                        $label .= $records[$i][trim($field)]." ";
                                $list[$records[$i][$this->_primary_key_column]] = $label;
                        }
                }
                return $list;
        }
	// get all records out associated with the primary key column(s) of the table and store them in an array with field names as indexes
  	public function get_all_array($sort_key = null, $offset = "0", $limit = "10000", $direction = "ASC", $search_string = null, $added_query = '', $added_query_type = 'AND')
  	{
		return $this->get_records($sort_key, $offset, $limit, $direction, $search_string, $added_query, $added_query_type, $called_from = 'get_all_array');
	}
	// step through all records out associated with the primary key column(s) of the table and stores the resource in $this->resource to be stepped through with $this->step();
  	public function step_all_array($sort_key = null, $offset = "0", $limit = "10000", $direction = "ASC", $search_string = null, $added_query = '', $added_query_type = 'AND')
  	{
		$this->_step_type = 'array';
		$this->_step_resource = null;
		$this->_step_number = 0;
		return $this->get_records($sort_key, $offset, $limit, $direction, $search_string, $added_query, $added_query_type, $called_from = 'step_array');
	}
	// get all records out associated with the table and store them in an array or classes
  	public function get_all_class($sort_key = null, $offset = "0", $limit = "10000", $direction = "ASC", $search_string = null, $added_query = '', $added_query_type = 'AND')
  	{
		return $this->get_records($sort_key, $offset, $limit, $direction, $search_string, $added_query, $added_query_type, $called_from = 'get_all_class');
	}
	// step through all records out associated with the primary key column(s) of the table and stores the resource in $this->resource to be stepped through with $this->step();
  	public function step_all_class($sort_key = null, $offset = "0", $limit = "10000", $direction = "ASC", $search_string = null, $added_query = '', $added_query_type = 'AND')
  	{
		$this->_step_type = 'class';
		$this->_step_resource = null;
		$this->_step_number = 0;
		return $this->get_records($sort_key, $offset, $limit, $direction, $search_string, $added_query, $added_query_type, $called_from = 'step_class');
	}

	// get records out where stored values match set values, return array of classes
	public function find($sort_key = null, $offset = "0", $limit = "10000", $direction = "ASC", $search_string = '', $added_query = '', $added_query_type = 'AND')
	{
		return $this->get_records($sort_key, $offset, $limit, $direction, $search_string, $added_query, $added_query_type, $called_from = 'find');
	}
	// count records where stored values match set values, return integer
	public function find_count($sort_key = null, $offset = "0", $limit = "10000", $direction = "ASC", $search_string = '', $added_query = '', $added_query_type = 'AND')
	{
		return $this->get_records($sort_key, $offset, $limit, $direction, $search_string, $added_query, $added_query_type, $called_from = 'count_find');
	}
	// get records out where stored values match set values, step through with $this->stop
	public function find_step($sort_key = null, $offset = "0", $limit = "10000", $direction = "ASC", $search_string = '', $added_query = '', $added_query_type = 'AND')
	{
		$this->_step_type = 'find';
		$this->_step_resource = null;
		$this->_step_number = 0;
		return $this->get_records($sort_key, $offset, $limit, $direction, $search_string, $added_query, $added_query_type, $called_from = 'step_find');
	}

	// get one record out where stored values match set values, sets this equal to the variable
	public function get_one($search_string = '', $added_query = '', $added_query_type = 'AND')
	{
		return $this->get_records(null, null, null, null, $search_string, $added_query, $added_query_type, $called_from = 'get_one');
	}


	// called from get_find_pagination() and get_all_pagination()
	public function get_pagination($url, $offset, $limit, $style, $search_string = null, $number_of_middle_pages = 0, $link_style = null, $added_query = '', $added_query_type = 'AND', $called_from = 'get_all_pagination')
	{
		if (@!isset($url) || @!isset($offset) || @!isset($limit))
		{
			$this->_last_error = "get_pagination requires url, offset, and limit.  one of these things is not set.";
			return false;
		}

		// if $url ends in =, they've already named the offset variable, just add the number, otherwise, add the GET variable "offset"
		if (is_string($url) && substr(trim($url), strlen($url)-1, 1) != '=')
			$url .= "&offset=";


		if ($called_from = 'get_find_pagination')
			$statement = $this->get_find_sql($search_string, $added_query, $added_query_type);
		else
			$statement = $this->get_all_sql($search_string, $added_query, $added_query_type);

		$sql = "SELECT count(*) FROM ($statement) as t";
		$result = pg_query($this->_dbh, $sql);
		$records = pg_fetch_row($result);

		$total_records = $records[0];

		$last = $total_records;
		$first = 1;
		if ($total_records == 0)
			$first = $last = 0;
		if ($total_records <= $limit)
			return array("n_link"=>"","p_link"=>"","total"=>"$total_records","first"=>"$first","last"=>"$last");

		$next_records = $total_records - $offset - $limit;
		if ($next_records > $limit)
			$next_records = $limit;

		$prev_records = $offset;
		if ($prev_records > $limit)
			$prev_records = $limit;

		$prev_offset = $offset+$limit;
		$next_offset = $offset-$prev_records;

		$last = $offset + $limit;
		if ($last > $total_records)
			$last = $total_records;

		$pagination = array();
		$pagination['total'] = $total_records;
		$pagination['first'] = ($offset + 1);
		$pagination['last'] = $last;
                if ($next_records > 0)
                        if ($link_style == '2arrow')
                                $pagination['n_link'] = "<a href=\"$url"."$prev_offset\" $style>Next $next_records &gt;&gt;</a>";
                        else
                                $pagination['n_link'] = "<a href=\"$url"."$prev_offset\" $style>Next $next_records</a>";
                else
                        $pagination['n_link'] = "";

                if ($prev_records > 0)
                        if ($link_style == '2arrow')
                                $pagination['p_link'] = "<a href=\"$url"."$next_offset\" $style>&lt;&lt; Prev $prev_records</a>";
                        else
                                $pagination['p_link'] = "<a href=\"$url"."$next_offset\" $style>Prev $prev_records</a>";
                else
                        $pagination['p_link'] = "";


		if ($total_records > $limit*2 && $number_of_middle_pages > 0)
		{
			// create middle links
			$current_page = $offset/$limit; // find out which page i'm on
			$total_pages = floor($total_records/$limit); // find out how many pages there are

			$current_page++;
			$total_pages++;

			// see if we need to put the first page on here
			if ($current_page - $number_of_middle_pages > 1)
			{
				$pagination['middle_links'][] = "<a href=\"$url"."0\" $style>1</a>";
				if ($current_page - $number_of_middle_pages > 2)
					$pagination['middle_links'][] = "...";
			}

			// do the middle links
			for ($x = $current_page - $number_of_middle_pages; $x < $current_page; $x++)
			{
				if ($x <= 0 )
					continue;
				$temp_offset = $limit*($x-1);
				if ($temp_offset >= $total_records)
					continue;
				$pagination['middle_links'][] = "<a href=\"$url"."$temp_offset\" $style>$x</a>";
			}
			$pagination['middle_links'][] = "<span $style>$current_page</span>";
			for ($x = $current_page + 1; $x < $current_page + $number_of_middle_pages + 1; $x++)
			{
				if ($x > $total_pages)
					break;
				$temp_offset = $limit*($x-1);
				if ($temp_offset >= $total_records)
					continue;
				$pagination['middle_links'][] = "<a href=\"$url"."$temp_offset\" $style>$x</a>";
			}

			$total_pages--;

			// see if we need to put the last page on here
			if ($current_page + $number_of_middle_pages < $total_pages)
			{
				$offset = ($total_pages-1)*$limit;
				if ($offset < $total_records)
				{
					if ($current_page + $number_of_middle_pages + 1 < $total_pages)
						$pagination['middle_links'][] = "...";
					$pagination['middle_links'][] = "<a href=\"$url"."$offset\" $style>$total_pages</a>";
				}

			}

			$pagination['middle_links'] = implode(" ", $pagination['middle_links']);
		}

		return $pagination;
	}

	// gets pagination variables for the get_all_?? functions
	public function get_all_pagination($url, $offset, $limit, $style, $search_string = null, $number_of_middle_pages = 0, $link_style = null, $added_query = '', $added_query_type = 'AND')
	{
		return $this->get_pagination($url, $offset, $limit, $style, $search_string, $number_of_middle_pages, $link_style, $added_query, $added_query_type, $called_from = 'get_all_pagination');
	}
	// gets pagination variables for the get_find_?? functions
	public function get_find_pagination($url, $offset, $limit, $style, $search_string = null, $number_of_middle_pages = 0, $link_style = null, $added_query = '', $added_query_type = 'AND')
	{
		return $this->get_pagination($url, $offset, $limit, $style, $search_string, $number_of_middle_pages, $link_style, $added_query, $added_query_type, $called_from = 'get_find_pagination');
	}

	// insert class values as a new record in the table
	public function insert($generate_id = 1)
	{
		if ($generate_id && $this->_primary_key)
		{
			$sql = '';
                        if ($this->_schema && $this->_schema != 'public')
                                $sql .= "set search_path TO $this->_schema;";
                        $sql .= "SELECT ".$this->_columns[$this->_primary_key_column]['default'];
			$result = pg_query($this->_dbh, $sql);
			$row = pg_fetch_row($result);
			if (!$this->{"set_".$this->_primary_key_column}($row[0]))
			{
				$this->_last_error = "Failed to generate a primary key.<br>\n$sql<br>\nGenerated: $row[0] -- does the sequence have proper ownership/permissions?";
				return false;
			}
		}
		else
		{
			#if (isset($this->{$this->_primary_key_column}))
				#unset($this->{$this->_primary_key_column});
		}
		foreach($this->_columns as $name=>$v)
		{
			if ($v['notnull'] && $v['default'] == '' && !isset($this->{$name})) // if it can't be null, doesn't have a default value, and isn't set, return false
			{
				$this->_last_error = "Field: $name is null.  Database requires this field not be null";
				return false;
			}
		}

		$sql = "INSERT INTO $this->_prepend_dot_q\"$this->_name\" (";
		foreach($this->_columns as $name=>$v)
			if (@isset($this->{$name})) $sql .= "\"$name\", ";
		$sql = substr($sql, 0, strlen($sql)-2);
		$sql .= ") VALUES (";
		foreach($this->_columns as $name=>$v)
			if (@isset($this->{$name})) $sql .= "'".pg_escape_string($this->{$name})."', ";;
		$sql = substr($sql, 0, strlen($sql)-2);
		$sql .= ");";

		if (!pg_query($this->_dbh, $sql))
		{
			$this->_last_error = "Failed to execute query.<br>\n$sql<br>\nMessage from Postgresql: ".pg_last_error($this->_dbh);
			return false;
		}

		return true;
	}
	// delete exactly 1 record from table, use multiple_delete() to delete multiple records
	public function delete()
	{
		$params = array();
		foreach($this->_columns as $name=>$v)
			if (@isset($this->{$name})) $params[] .= "\"$name\" = '".pg_escape_string($this->{$name})."'";
		if (count($params) == 0)
		{
			$this->_last_error = "No fields were given values.  At least one field needs a value so we can search for a record";
			return false;
		}
		$param_string = implode(" AND ", $params);

		$sql = "SELECT count(*) FROM $this->_prepend_dot_q\"$this->_name\" WHERE $param_string;";
		if (!$query = pg_query($this->_dbh, $sql))
		{
			$this->_last_error = "Unable to select how many unique rows match your query.<br>\n$sql<br>\nMessage from Postgresql: ".pg_last_error($this->_dbh);
			return false;
		}
		$row = pg_fetch_row($query);
		if ($row[0] != '1')
		{
			$this->_last_error = "Query must return exactly one row in order to delete the row.  Your query returned $row[0] rows";
			return false;
		}

        $sql = "DELETE FROM $this->_prepend_dot_q\"$this->_name\" WHERE $param_string;";
	    if (!pg_query($this->_dbh, $sql))
		{
			$this->_last_error = "Delete query failed.<br>\n$sql<br>\nMessage from Postgresql: ".pg_last_error($this->_dbh);
	                return false;
		}

		return true;

	}
	// deletes any number of records
	public function multiple_delete()
	{
		$params = array();
		foreach($this->_columns as $name=>$v)
			if (@isset($this->{$name})) $params[] .= "\"$name\" = '".pg_escape_string($this->{$name})."'";
		if (count($params) == 0)
		{
			$this->_last_error = "No fields were given values.  At least one field needs a value so we can search for a record";
			return false;
		}
		$param_string = implode(" AND ", $params);

        	$sql = "DELETE FROM $this->_prepend_dot_q\"$this->_name\" WHERE $param_string;";
	        if (!pg_query($this->_dbh, $sql))
		{
			$this->_last_error = "Delete query failed.<br>\n$sql<br>\nMessage from Postgresql: ".pg_last_error($this->_dbh);
	                return false;
		}

		return true;
	}
	// updates a record in the database that matches the primary key(s) of the table.  1 or more primary keys are required
	public function update()
	{
		if ($this->_primary_key)
		{
			$where = "WHERE $this->_primary_key_column = '".pg_escape_string($this->{$this->_primary_key_column})."'";
		}
		elseif ($this->_primary_keys)
		{
			$where = "WHERE ";
			foreach($this->_primary_key_columns as $c)
				$where .= "$c = '".pg_escape_string($this->{$c})."' AND ";
			$where = substr($where, 0, strlen($where)-5);
		}
		else
		{
			$this->_last_error = "Cannot update records when no primary keys exist";
			return false;
		}

		foreach($this->_columns as $name=>$v)
		{
			if ($v['notnull'] && $v['default'] == '' && !isset($this->{$name})) // if it can't be null, doesn't have a default value, and isn't set, return false
			{
				$this->_last_error = "Field: ".$this->{$name}." is null.  Database requires this field not be null";
				return false;
			}
		}

		$sql = "UPDATE $this->_prepend_dot_q\"$this->_name\" SET ";
		foreach($this->_columns as $name=>$v)
		{
			if ($name == $this->_primary_key_column || in_array($name, $this->_primary_key_columns))
				continue;
            if (isset($this->{$name}) && is_string($this->{$name}))
				$sql .= "\"$name\" = '".pg_escape_string(trim($this->{$name}))."', ";
			else
				$sql .= "\"$name\" = NULL, ";
		}
	        $sql = substr($sql, 0, strlen($sql)-2);
		$sql .= " $where;";

		if (!pg_query($this->_dbh, $sql))
		{
			$this->_last_error = "Failed to execute query.<br>\n$sql<br>\nMessage from Postgresql: ".pg_last_error($this->_dbh);
	                return false;
		}
        return true;
	}
	// inserts if primary keys do not exist, otherwise updates
	public function store()
	{
		if ($this->_primary_key)
		{
			if (strlen(trim($this->{$this->_primary_key_column})) == 0)
                                $where = "WHERE $this->_primary_key_column IS NULL";
                        else
                                $where = "WHERE $this->_primary_key_column = '".pg_escape_string($this->{$this->_primary_key_column})."'";
		}
		elseif ($this->_primary_keys)
		{
			$where = "WHERE ";
			foreach($this->_primary_key_columns as $c)
				$where .= "$c = '".pg_escape_string($this->{$c})."' AND ";
			$where = substr($where, 0, strlen($where)-5);
		}
		else
		{
			$this->_last_error = "Cannot update records when no primary keys exist.  If this is a new record, use the insert function.";
			return false;
		}
		$sql = "SELECT count(*) FROM $this->_prepend_dot_q\"$this->_name\" $where";
		if (!$query = pg_query($this->_dbh, $sql))
		{
			// the query will fail if the primary key is an integer and it isn't set, if it isn't set, it doesn't exist.
			return $this->insert();
		}
		$row = pg_fetch_row($query);
		if ($row[0] == '1')
			return $this->update();
		elseif ($row[0] == '0')
			return $this->insert();
		else
		{
			$this->_last_error = "Multiple records were returned from a query of primary keys...  I don't know how this is possible.";
			return false;
		}

	}
	// step through the elements in $this->_step_resource
	public function step()
	{
		// $this->_step_type = 'array' or 'class' or 'find'
		// $this->_step_resource = $database_result
		// $this->_step_number = the current record we're on in the resource;

		if ($this->_step_type == 'array')
		{
			if (!$array = pg_fetch_assoc($this->_step_resource, $this->_step_number))
			{
				$this->_step_type = null; $this->_step_resource = null; $this->_step_number = null;
				return false;
			}
			$this->_step_number++;
			return $array;
		}
		if ($this->_step_type == 'class' || $this->_step_type == 'find')
		{
			if (!$array = pg_fetch_assoc($this->_step_resource, $this->_step_number))
			{
				$this->_step_type = null; $this->_step_resource = null; $this->_step_number = null;
				return false;
			}
			if (strlen($this->_schema) > 0 && $this->_schema != 'public')
				$table_name = "$this->_schema"."__$this->_name";
			else
				$table_name = $this->_name;
			if (strlen($this->_namespace) > 0)
				$table_name = "$this->_namespace\\$table_name";
			$c = new $table_name;
			$c->set_from_array_unsafe($array);
			$this->_step_number++;
			return $c;

		}
		else
			return false;

	}
