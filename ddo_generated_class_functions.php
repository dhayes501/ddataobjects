<?php
function formmod($string)
{
        $string = "a$string";
        $string2 = "";
        for ($i = 0; $i < strlen($string); $i++)
        {
                if ($string[$i] == "'") $string2 .= "&#39;";
                elseif($string[$i] == "\"") $string2 .= "&#34;";
                elseif($string[$i] == "<") $string2 .= "&lt;";
                elseif($string[$i] == ">") $string2 .= "&gt;";
                else $string2 .= $string[$i];
        }
        $string3 = substr($string2, 1, strlen($string2)-1);
        return $string3;
}
function displaymod($string)
{
	$string2 = str_replace("\n", "<br>", $string);
	$string3 = str_replace("  ", "&nbsp;&nbsp;", $string2);
	return $string3;
}
function create_random_string($length = '15', $character_set = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789")
{
	$string = "";
	for($i = 0; $i < $length; $i++)
		$string .= substr($character_set, mt_rand(0, strlen($character_set)-1), 1);
	return $string;
}
function display_currency($string, $decimal_places = 2)
{
	return "$".number_format($string, $decimal_places, '.', ',');
}
function is_int_val($value)
{
        $int = intval($value);
        return ("$int" == "$value");
}
function is_double_val($value)
{
        if (preg_match('/^[\d]*[\.]{0,1}[\d]*$/', $value))
                return true;
        return false;
}
function is_email_val($value)
{
    $email_regexp = "/^[_a-zA-Z0-9+-]+(\.[_a-zA-Z0-9+-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/";
    if (preg_match($email_regexp, $value))
        return true;
    return false;
}
function is_timestamp_val($t)
{
	$date = $time = null;
        $t = trim($t);
	if (strstr($t, " "))
		list($date, $time) = explode(" ", $t);
	else
		$date = $t;
	if (!$date = split_date($date))
		return false;
	if (!$time = is_time_val($time))
		$time = array("hour"=>1,"minute"=>1,"second"=>1,"suffix"=>'am');
	return array_merge($date, $time);
}
function is_time_val($time)
{
	// make sure there's no blank spaces before or after time
	$time = trim($time);

	// pull off the last two characters of the time
	$suffix = strtolower(substr($time, -2, 2));
	// test to see if they're 'am' or 'pm'
	if ($suffix != 'am' && $suffix != 'pm')
		$suffix = ''; // if they aren't, there is no suffix
	else
		$time = substr($time, 0, strlen($time)-2); // if they are, chop them off

	$time = str_replace('.', ':', $time);

	// if only the hour is set, put minutes on also
	if (preg_match('/^[\d]{1,2}$/', $time))
		$time = "$time:00";
	if (preg_match('/^[\d]{1,2}[:]{1}[\d]{1,2}$/', $time))
		$time = "$time:00";

	if (preg_match('/^[\d]{1,2}[:]{1}[\d]{1,2}[:]{1}[\d]{1,2}[:]{1}[\d]+$/', $time)) // if there's milliseconds
		list($h, $m, $s, $ms) = explode(":", $time);
	elseif (preg_match('/^[\d]{1,2}[:\.]{1}[\d]{1,2}[:\.]{1}[\d]{1,2}$/', $time)) // if there isn't
	{
		list($h,$m,$s) = explode(":", $time);
		$ms = '000000';
	}
	else // else its not valid
		return false;

	if ($suffix == 'pm' && $h < 12)
		$h+=12;
	if (!$s)
		$s = '00';
	if ($h > 23 || $m > 59 || $s > 59)
		return false;

	$array['hour'] = $h;
	$array['minute'] = $m;
	$array['second'] = $s;
	$array['milliseconds'] = $ms;
	$array['suffix'] = $suffix;
	// return hh:mm:ss
	return $array;
}
function split_date($date)
{
	$array = array('month'=>null,'day'=>null,'year'=>null);

	if (preg_match('/[a-z][A-Z]/i', $date))
	{
		// if they enter the month spelled out, pull it out of the date and set the month variable
		if (strstr(strtolower($date), 'jan')){	$array['month'] = '01'; $date = preg_replace('/[a-zA-Z]+/', "-", $date); }
		elseif (strstr(strtolower($date), 'feb'))	{ $array['month'] = '02'; $date = preg_replace('/[a-zA-Z]+/', "-", $date); }
		elseif (strstr(strtolower($date), 'mar'))	{ $array['month'] = '03'; $date = preg_replace('/[a-zA-Z]+/', "-", $date); }
		elseif (strstr(strtolower($date), 'apr'))	{ $array['month'] = '04'; $date = preg_replace('/[a-zA-Z]+/', "-", $date); }
		elseif (strstr(strtolower($date), 'may'))	{ $array['month'] = '05'; $date = preg_replace('/[a-zA-Z]+/', "-", $date); }
		elseif (strstr(strtolower($date), 'jun'))	{ $array['month'] = '06'; $date = preg_replace('/[a-zA-Z]+/', "-", $date); }
		elseif (strstr(strtolower($date), 'jul'))	{ $array['month'] = '07'; $date = preg_replace('/[a-zA-Z]+/', "-", $date); }
		elseif (strstr(strtolower($date), 'aug'))	{ $array['month'] = '08'; $date = preg_replace('/[a-zA-Z]+/', "-", $date); }
		elseif (strstr(strtolower($date), 'sep'))	{ $array['month'] = '09'; $date = preg_replace('/[a-zA-Z]+/', "-", $date); }
		elseif (strstr(strtolower($date), 'oct'))	{ $array['month'] = '10'; $date = preg_replace('/[a-zA-Z]+/', "-", $date); }
		elseif (strstr(strtolower($date), 'nov'))	{ $array['month'] = '11'; $date = preg_replace('/[a-zA-Z]+/', "-", $date); }
		elseif (strstr(strtolower($date), 'dec'))	{ $array['month'] = '12'; $date = preg_replace('/[a-zA-Z]+/', "-", $date); }
		// they have to enter a 4 digit year with this  04 Jun 02 is pretty ambiguous...
		if (!preg_match('/[0-9]{4}/i', $date))
			return 0;
		preg_match('/[0-9]{4}/i', $date, $temp_year);  // pull the year out and store it in the 1st element of the array "temp_year"
		$array['year'] = $temp_year[0]; // now actually set the year
		$date = preg_replace('/[0-9]{4}/', "-", $date); // remove out the year so we can grab the day
		preg_match('/[0-9]{1,2}/i', $date, $temp_day);
		if ($day = (int) $temp_day[0])
			$array['day'] = $day;
		else
			$array['day'] = 1;
	}
	else
	{
		$date = str_replace("/", "-", $date);
		if(preg_match('/^[\d]{1,2}[-]{1}[\d]{1,2}[-]{1}[\d]{2,4}$/' , $date))
			list($array['month'], $array['day'], $array['year']) = explode("-", $date);
		elseif(preg_match('/^[\d]{2,4}[-]{1}[\d]{1,2}[-]{1}[\d]{1,2}$/' , $date))
			list($array['year'], $array['month'], $array['day']) = explode("-", $date);
		elseif(preg_match('/^[\d]{1,2}[-]{1}[\d]{1,4}$/' , $date))
		{
			list($array['month'], $array['year']) = explode("-", $date);
			$array['day'] = 1;
		}
		elseif(preg_match('/^[\d]{1,4}[-]{1}[\d]{1,2}$/' , $date))
		{
			list($array['year'], $array['month']) = explode("-", $date);
			$array['day'] = 1;
		}
		elseif(preg_match('/^[\d]{1,4}$/' , $date))
		{
			$array['year'] = $date;
			$array['month'] = 1;
			$array['day'] = 1;
		}
		else
			return 0;
		if ($array['month'] > 12 && $array['day'] <= 12)
		{
			$day_bak = $array['day'];
			$month_bak = $array['month'];
			$array['day'] = $month_bak;
			$array['month'] = $day_bak;
		}
	}

	if (strlen($array['month']) == 1)
		$array['month'] = '0'.$array['month'];
	if (strlen($array['day']) == 1)
		$array['day'] = '0'.$array['day'];

	if (!checkdate($array['month'], $array['day'], $array['year']))
		return 0;
	return $array;
}
function get_form_element($a)
{
	if (strlen($a['id']) == 0)
		$a['id'] = $a['column_name'];
        if ($a['type'] == 'select' || $a['type'] == 'radio')
        {
                if ($a['elements'] == 'states' || $a['elements'] == 'states_abbr')
                {
                        $a['elements'] = "|[Select One]||AK|Alaska||AL|Alabama||AR|Arkansas||AZ|Arizona||
                        CA|California||CO|Colorado||CT|Connecticut||DE|Deleware||DC|District of Columbia||FL|Florida||GA|Georgia||HI|Hawaii||ID|Idaho||
                        IL|Illinois||IN|Indiana||IA|Iowa||KS|Kansas||KY|Kentucky||LA|Louisiana||ME|Maine||MD|Maryland||MA|Massachusetts||MI|Michigan||
                        MN|Minnesota||MS|Mississippi||MO|Missouri||MT|Montana||NE|Nebraska||NV|Nevada||NH|New Hampshire||NJ|New Jersey||NM|New Mexico||
                        NY|New York||NC|North Carolina||ND|North Dakota||OH|Ohio||OK|Oklahoma||OR|Oregon||PA|Pennsylvania||RI|Rhode Island||
                        SC|South Carolina||SD|South Dakota||TN|Tennessee||TX|Texas||UT|Utah||VT|Vermont||VA|Virginia||WA|Washington||WV|West Virginia||
                        WI|Wisconsin||WY|Wyoming";
                }
		if ($a['elements'] == 'states_full')
                {
                        $a['elements'] = "|[Select One]||Alaska|Alaska||Alabama|Alabama||Arkansas|Arkansas||Arizona|Arizona||California|California||Colorado|Colorado||
			Connecticut|Connecticut||Deleware|Deleware||District of Columbia|District of Columbia||Florida|Florida||Georgia|Georgia||Hawaii|Hawaii||Idaho|Idaho||
                        Illinois|Illinois||Indiana|Indiana||Iowa|Iowa||Kansas|Kansas||Kentuck|Kentucky||Louisiana|Louisiana||Maine|Maine||Maryland|Maryland||
			Massachusetts|Massachusetts||MI|Michigan||Minnesota|Minnesota||Mississippi|Mississippi||Missouri|Missouri||Montana|Montana||Nebraska|Nebraska||
			Nevada|Nevada||New Hampshire|New Hampshire||New Jersey|New Jersey||New Mexico|New Mexico||New York|New York||North Carolina|North Carolina||
			North Dakota|North Dakota||Ohio|Ohio||Oklahoma|Oklahoma||Oregon|Oregon||Pennsylvania|Pennsylvania||Rhode Island|Rhode Island||South Carolina|South Carolina||
			South Dakota|South Dakota||Tennessee|Tennessee||Texas|Texas||Utah|Utah||Vermont|Vermont||Virginia|Virginia||Washington|Washington||
			West VirginiaV|West Virginia||Wisconsin|Wisconsin||Wyoming|Wyoming";
                }
		if ($a['elements'] == 'countries' || $a['elements'] == 'countries_abbr')
                {
                        $a['elements'] = "|[Select One]||AF|Afghanistan||AL|Albania||DZ|Algeria||AS|American Samoa||AD|Andorra||AG|Angola||AI|Anguilla||AG|Antigua & Barbuda||
			AR|Argentina||AA|Armenia||AW|Aruba||AU|Australia||AT|Austria||AZ|Azerbaijan||BS|Bahamas||BH|Bahrain||BD|Bangladesh||BB|Barbados||BY|Belarus||BE|Belgium||
			BZ|Belize||BJ|Benin||BM|Bermuda||BT|Bhutan||BO|Bolivia||BL|Bonaire||BA|Bosnia & Herzegovina||BW|Botswana||BR|Brazil||BC|British Indian Ocean Ter||
			BN|Brunei||BG|Bulgaria||BF|Burkina Faso||BI|Burundi||KH|Cambodia||CM|Cameroon||CA|Canada||IC|Canary Islands||CV|Cape Verde||KY|Cayman Islands||
			CF|Central African Republic||TD|Chad||CD|Channel Islands||CL|Chile||CN|China||CI|Christmas Island||CS|Cocos Island||CO|Colombia||CC|Comoros||CG|Congo||
			CK|Cook Islands||CR|Costa Rica||CT|Cote D'Ivoire||HR|Croatia||CU|Cuba||CB|Curacao||CY|Cyprus||CZ|Czech Republic||DK|Denmark||DJ|Djibouti||DM|Dominica||
			DO|Dominican Republic||TM|East Timor||EC|Ecuador||EG|Egypt||SV|El Salvador||GQ|Equatorial Guinea||ER|Eritrea||EE|Estonia||ET|Ethiopia||FA|Falkland Islands||
			FO|Faroe Islands||FJ|Fiji||FI|Finland||FR|France||GF|French Guiana||PF|French Polynesia||FS|French Southern Ter||GA|Gabon||GM|Gambia||GE|Georgia||DE|Germany||
			GH|Ghana||GI|Gibraltar||GB|Great Britain||GR|Greece||GL|Greenland||GD|Grenada||GP|Guadeloupe||GU|Guam||GT|Guatemala||GN|Guinea||GY|Guyana||HT|Haiti||HW|Hawaii||
			HN|Honduras||HK|Hong Kong||HU|Hungary||IS|Iceland||IN|India||ID|Indonesia||IA|Iran||IQ|Iraq||IR|Ireland||IM|Isle of Man||IL|Israel||IT|Italy||JM|Jamaica||JP|Japan||
			JO|Jordan||KZ|Kazakhstan||KE|Kenya||KI|Kiribati||NK|Korea North||KS|Korea South||KW|Kuwait||KG|Kyrgyzstan||LA|Laos||LV|Latvia||LB|Lebanon||LS|Lesotho||LR|Liberia||
			LY|Libya||LI|Liechtenstein||LT|Lithuania||LU|Luxembourg||MO|Macau||MK|Macedonia||MG|Madagascar||MY|Malaysia||MW|Malawi||MV|Maldives||ML|Mali||MT|Malta||
			MH|Marshall Islands||MQ|Martinique||MR|Mauritania||MU|Mauritius||ME|Mayotte||MX|Mexico||MI|Midway Islands||MD|Moldova||MC|Monaco||MN|Mongolia||MS|Montserrat||
			MA|Morocco||MZ|Mozambique||MM|Myanmar||NA|Nambia||NU|Nauru||NP|Nepal||AN|Netherland Antilles||NL|Netherlands (Holland, Europe)||NV|Nevis||NC|New Caledonia||
			NZ|New Zealand||NI|Nicaragua||NE|Niger||NG|Nigeria||NW|Niue||NF|Norfolk Island||NO|Norway||OM|Oman||PK|Pakistan||PW|Palau Island||PS|Palestine||PA|Panama||
			PG|Papua New Guinea||PY|Paraguay||PE|Peru||PH|Philippines||PO|Pitcairn Island||PL|Poland||PT|Portugal||PR|Puerto Rico||QA|Qatar||RE|Reunion||RO|Romania||
			RU|Russia||RW|Rwanda||NT|St Barthelemy||EU|St Eustatius||HE|St Helena||KN|St Kitts-Nevis||LC|St Lucia||MB|St Maarten||PM|St Pierre & Miquelon||VC|St Vincent & Grenadines||
			SP|Saipan||SO|Samoa||AS|Samoa American||SM|San Marino||ST|Sao Tome & Principe||SA|Saudi Arabia||SN|Senegal||SC|Seychelles||SS|Serbia & Montenegro||SL|Sierra Leone||
			SG|Singapore||SK|Slovakia||SI|Slovenia||SB|Solomon Islands||OI|Somalia||ZA|South Africa||ES|Spain||LK|Sri Lanka||SD|Sudan||SR|Suriname||SZ|Swaziland||SE|Sweden||
			CH|Switzerland||SY|Syria||TA|Tahiti||TW|Taiwan||TJ|Tajikistan||TZ|Tanzania||TH|Thailand||TG|Togo||TK|Tokelau||TO|Tonga||TT|Trinidad & Tobago||TN|Tunisia||
			TR|Turkey||TU|Turkmenistan||TC|Turks & Caicos Is||TV|Tuvalu||UG|Uganda||UA|Ukraine||AE|United Arab Emirates||GB|United Kingdom||US|United States of America||
			UY|Uruguay||UZ|Uzbekistan||VU|Vanuatu||VS|Vatican City State||VE|Venezuela||VN|Vietnam||VB|Virgin Islands (Brit)||VA|Virgin Islands (USA)||WK|Wake Island||
			WF|Wallis & Futana Is||YE|Yemen||ZR|Zaire||ZM|Zambia||ZW|Zimbabwe";
                }
		if ($a['elements'] == 'countries_full')
		{
			$a['elements'] = "|[Select One]||Afganistan|Afghanistan||Albania|Albania||Algeria|Algeria||American Samoa|American Samoa||Andorra|Andorra||Angola|Angola||Anguilla|Anguilla||
			Antigua & Barbuda|Antigua & Barbuda||Argentina|Argentina||Armenia|Armenia||Aruba|Aruba||Australia|Australia||Austria|Austria||Azerbaijan|Azerbaijan||Bahamas|Bahamas||Bahrain|Bahrain||
			Bangladesh|Bangladesh||Barbados|Barbados||Belarus|Belarus||Belgium|Belgium||Belize|Belize||Benin|Benin||Bermuda|Bermuda||Bhutan|Bhutan||Bolivia|Bolivia||Bonaire|Bonaire||
			Bosnia & Herzegovina|Bosnia & Herzegovina||Botswana|Botswana||Brazil|Brazil||British Indian Ocean Ter|British Indian Ocean Ter||Brunei|Brunei||Bulgaria|Bulgaria||Burkina Faso|Burkina Faso||
			Burundi|Burundi||Cambodia|Cambodia||Cameroon|Cameroon||Canada|Canada||Canary Islands|Canary Islands||Cape Verde|Cape Verde||Cayman Islands|Cayman Islands||Central African Republic|Central African Republic||
			Chad|Chad||Channel Islands|Channel Islands||Chile|Chile||China|China||Christmas Island|Christmas Island||Cocos Island|Cocos Island||Colombia|Colombia||Comoros|Comoros||Congo|Congo||Cook Islands|Cook Islands||
			Costa Rica|Costa Rica||Cote DIvoire|Cote D'Ivoire||Croatia|Croatia||Cuba|Cuba||Curaco|Curacao||Cyprus|Cyprus||Czech Republic|Czech Republic||Denmark|Denmark||Djibouti|Djibouti||Dominica|Dominica||
			Dominican Republic|Dominican Republic||East Timor|East Timor||Ecuador|Ecuador||Egypt|Egypt||El Salvador|El Salvador||Equatorial Guinea|Equatorial Guinea||Eritrea|Eritrea||Estonia|Estonia||Ethiopia|Ethiopia||
			Falkland Islands|Falkland Islands||Faroe Islands|Faroe Islands||Fiji|Fiji||Finland|Finland||France|France||French Guiana|French Guiana||French Polynesia|French Polynesia||French Southern Ter|French Southern Ter||
			Gabon|Gabon||Gambia|Gambia||Georgia|Georgia||Germany|Germany||Ghana|Ghana||Gibraltar|Gibraltar||Great Britain|Great Britain||Greece|Greece||Greenland|Greenland||Grenada|Grenada||Guadeloupe|Guadeloupe||
			Guam|Guam||Guatemala|Guatemala||Guinea|Guinea||Guyana|Guyana||Haiti|Haiti||Hawaii|Hawaii||Honduras|Honduras||Hong Kong|Hong Kong||Hungary|Hungary||Iceland|Iceland||India|India||Indonesia|Indonesia||
			Iran|Iran||Iraq|Iraq||Ireland|Ireland||Isle of Man|Isle of Man||Israel|Israel||Italy|Italy||Jamaica|Jamaica||Japan|Japan||Jordan|Jordan||Kazakhstan|Kazakhstan||Kenya|Kenya||Kiribati|Kiribati||
			Korea North|Korea North||Korea Sout|Korea South||Kuwait|Kuwait||Kyrgyzstan|Kyrgyzstan||Laos|Laos||Latvia|Latvia||Lebanon|Lebanon||Lesotho|Lesotho||Liberia|Liberia||Libya|Libya||Liechtenstein|Liechtenstein||
			Lithuania|Lithuania||Luxembourg|Luxembourg||Macau|Macau||Macedonia|Macedonia||Madagascar|Madagascar||Malaysia|Malaysia||Malawi|Malawi||Maldives|Maldives||Mali|Mali||Malta|Malta||
			Marshall Islands|Marshall Islands||Martinique|Martinique||Mauritania|Mauritania||Mauritius|Mauritius||Mayotte|Mayotte||Mexico|Mexico||Midway Islands|Midway Islands||Moldova|Moldova||Monaco|Monaco||
			Mongolia|Mongolia||Montserrat|Montserrat||Morocco|Morocco||Mozambique|Mozambique||Myanmar|Myanmar||Nambia|Nambia||Nauru|Nauru||Nepal|Nepal||Netherland Antilles|Netherland Antilles||
			Netherlands|Netherlands (Holland, Europe)||Nevis|Nevis||New Caledonia|New Caledonia||New Zealand|New Zealand||Nicaragua|Nicaragua||Niger|Niger||Nigeria|Nigeria||Niue|Niue||Norfolk Island|Norfolk Island||
			Norway|Norway||Oman|Oman||Pakistan|Pakistan||Palau Island|Palau Island||Palestine|Palestine||Panama|Panama||Papua New Guinea|Papua New Guinea||Paraguay|Paraguay||Peru|Peru||Phillipines|Philippines||
			Pitcairn Island|Pitcairn Island||Poland|Poland||Portugal|Portugal||Puerto Rico|Puerto Rico||Qatar|Qatar||Reunion|Reunion||Romania|Romania||Russia|Russia||Rwanda|Rwanda||St Barthelemy|St Barthelemy||
			St Eustatius|St Eustatius||St Helena|St Helena||St Kitts-Nevis|St Kitts-Nevis||St Lucia|St Lucia||St Maarten|St Maarten||St Pierre & Miquelon|St Pierre & Miquelon||
			St Vincent & Grenadines|St Vincent & Grenadines||Saipan|Saipan||Samoa|Samoa||Samoa American|Samoa American||San Marino|San Marino||Sao Tome & Principe|Sao Tome & Principe||Saudi Arabia|Saudi Arabia||
			Senegal|Senegal||Seychelles|Seychelles||Serbia & Montenegro|Serbia & Montenegro||Sierra Leone|Sierra Leone||Singapore|Singapore||Slovakia|Slovakia||Slovenia|Slovenia||Solomon Islands|Solomon Islands||
			Somalia|Somalia||South Africa|South Africa||Spain|Spain||Sri Lanka|Sri Lanka||Sudan|Sudan||Suriname|Suriname||Swaziland|Swaziland||Sweden|Sweden||Switzerland|Switzerland||Syria|Syria||Tahiti|Tahiti||
			Taiwan|Taiwan||Tajikistan|Tajikistan||Tanzania|Tanzania||Thailand|Thailand||Togo|Togo||Tokelau|Tokelau||Tonga|Tonga||Trinidad & Tobago|Trinidad & Tobago||Tunisia|Tunisia||Turkey|Turkey||
			Turkmenistan|Turkmenistan||Turks & Caicos Is|Turks & Caicos Is||Tuvalu|Tuvalu||Uganda|Uganda||Ukraine|Ukraine||United Arab Erimates|United Arab Emirates||United Kingdom|United Kingdom||
			United States of America|United States of America||Uraguay|Uruguay||Uzbekistan|Uzbekistan||Vanuatu|Vanuatu||Vatican City State|Vatican City State||Venezuela|Venezuela||Vietnam|Vietnam||
			Virgin Islands (Brit)|Virgin Islands (Brit)||Virgin Islands (USA)|Virgin Islands (USA)||Wake Island|Wake Island||Wallis & Futana Is|Wallis & Futana Is||Yemen|Yemen||Zaire|Zaire||Zambia|Zambia||Zimbabwe|Zimbabwe";
		}
        }



	if ($a['display'])
	{
		if (is_array($a['value']))
		{
			$x = 0;
			$string = '';
			foreach($a['value'] as $val)
			{
				$string .= "<input type=hidden name=\"$a[table_name][$a[column_name]][]\" id=\"$a[id]_$x value=\"".formmod($val)."\" $a[additional_params]>";
				$x++;
			}
		}
		else
		{
			if (($a['type'] == 'select' || $a['type'] == 'radio') && $a['value'] == '')
				$string = "<input type=hidden name=\"$a[table_name][$a[column_name]]\" id=\"$a[id]\" value=\" \" $a[additional_params]>";
			else
				$string = "<input type=hidden name=\"$a[table_name][$a[column_name]]\" id=\"$a[id]\" value=\"".formmod($a['value'])."\" $a[additional_params]>";
		}
		if ($a['display'] == 'hidden')
			return $string;

		if ($a['type'] == 'password')
		{
			if ($a['value'] != '')
				$string .= "********";
			else
				$string .= "<i>unchanged<i>";
		}
		elseif($a['type'] == 'select' || $a['type'] == 'radio')
		{
			if (!is_array($a['value']))
				$a['value'] = explode("||", $a['value']);
			if (is_array($a['elements']))
	                {
	                        foreach($a['elements'] as $k=>$v)
	                        {
	                                $k = str_replace("|", "][}{", $k);
	                                $v = str_replace("|", "][}{", $v);
	                                $values[] = "$k|$v";
	                        }
	                }
	                else
	                {
	                        $a['elements'] = str_replace("\\|", "][}{", $a['elements']);
	                        $values = explode("||", $a['elements']);
	                }
			foreach($values as $v)
			{
				if (strstr($v, "|"))
					list ($option, $val) = explode("|", $v);
				else
					$option = $val = $v;
				$option = trim(str_replace("][}{", "|", $option));
				$val = trim(str_replace("][}{", "|", $val));
				if (strlen($option) == 0)
					continue;
				if (in_array($option, $a['value']))$string .= "$val &nbsp; - &nbsp; ";
			}
			if (strstr(" &nbsp; - &nbsp;  ", substr($string, strlen($string) - 16, strlen($string))))
				$string = substr($string, 0, strlen($string) - 17);

		}
		elseif($a['type'] == 'checkbox')
		{
			if (strtoupper($a['value']) == 'TRUE' || strtoupper($a['value']) == 'T' || $a['value'] == 1)
				$string .= "<span style=\"color:green\">TRUE</span>";
			else
				$string .= "<span style=\"color:red\">FALSE</span>";
		}

		else
			$string .= str_replace("\n", "<br>", str_replace("  ", "&nbsp;&nbsp;", ($a['value'])));
		return $string;
	}
	if ($a['type'] == 'textbox')
	{
		if (@!$a['column_length']) $a['column_length'] = '';
		return "<input type=\"text\" name=\"$a[table_name][$a[column_name]]\" id=\"$a[id]\" value=\"".formmod($a['value'])."\" size=\"$a[size]\" maxlength=\"$a[column_length]\" $a[additional_params]>";
	}
	elseif ($a['type'] == 'password')
	{
		return "<input type=\"password\" name=\"$a[table_name][$a[column_name]]\" id=\"$a[id]\" value=\"\" size=\"$a[size]\" maxlength=\"$a[column_length]\" $a[additional_params]>";
	}
	elseif($a['type'] == 'textarea')
	{
		return "<textarea name=\"$a[table_name][$a[column_name]]\" id=\"$a[id]\" rows=\"$a[rows]\" cols=\"$a[columns]\" $a[additional_params]>$a[value]</textarea>";
	}
	elseif($a['type'] == 'select')
	{
                if (!is_array($a['value']))
                        $a['value'] = explode("||", $a['value']);

                $string = "\n<select name=\"$a[table_name][$a[column_name]]";
                if (strstr(strtolower($a['additional_params']), 'multiple') || $a['multiple'])
                        $string .= "[]";
                $string .= "\" id=\"$a[id]\" ";
                if ($a['multiple']) $string .= "multiple ";
                $string .= "$a[additional_params]>\n";
                if (is_array($a['elements']))
                {
                        foreach($a['elements'] as $k=>$v)
                        {
                                $k = str_replace("|", "][}{", $k);
                                $v = str_replace("|", "][}{", $v);
                                $values[] = "$k|$v";
                        }
                }
                else
                {
                        $a['elements'] = str_replace("\\|", "][}{", $a['elements']);
                        $values = explode("||", $a['elements']);
                }
                foreach($values as $v)
                {
                        if (strlen(trim($v)) == 0)
                                continue;
                        if (strstr($v, "|"))
                                list ($option, $val) = explode("|", $v);
                        else
                                $option = $val = $v;
                        if (!isset($option))
                                continue;
                        $option = trim(str_replace("][}{", "|", $option));
                        $val = trim(str_replace("][}{", "|", $val));
                        $string .= "<option value=\"$option\"";
                        if (in_array($option, $a['value'])) $string .= " selected";
                        $string .= ">$val</option>\n";
                }
                $string .= "</select>\n";
                return $string;
	}
	elseif($a['type'] == 'radio')
	{
		$string = "\n";

                if (!is_array($a['value']))
                        $a['value'] = explode("||", $a['value']);

                if (is_array($a['elements']))
                {
                        foreach($a['elements'] as $k=>$v)
                        {
                                $k = str_replace("|", "][}{", $k);
                                $v = str_replace("|", "][}{", $v);
                                $values[] = "$k|$v";
                        }
                }
                else
                {
                        $a['elements'] = str_replace("\\|", "][}{", $a['elements']);
                        $values = explode("||", $a['elements']);
                }
                foreach($values as $v)
                {
                        if (strstr($v, "|"))
                                list ($option, $val) = explode("|", $v);
                        else
                                $option = $val = $v;
			if (!isset($option) || (strlen($option) == 0 && strlen($value) == 0))
				continue;
                        $option = trim(str_replace("][}{", "|", $option));
                        $val = trim(str_replace("][}{", "|", $val));
                        $string .= "<input type=radio name=\"$a[table_name][$a[column_name]]\" id=\"$a[id]_$option\" value=\"$option\"";
                        if (in_array($option, $a['value'])) $string .= " checked";
                        $string .= " $a[additional_params]> $val $a[spacer]\n ";
                }
                return $string;
	}
	elseif($a['type'] == 'checkbox')
        {
                $string = "\n<input type=checkbox name=\"$a[table_name][$a[column_name]]\" id=\"$a[id]\" value=\"1\" $a[additional_params]";
                if ($a['value'] == '1' || strtoupper($a['value']) == 'T' || strtoupper($a['value']) == 'TRUE')
                        $string .= " checked";
                $string .= ">";
		return $string;
        }
	elseif ($a['type'] == 'hidden')
	{
		if (is_array($a['value']))
		{
			$x = 0;
			$string = '';
			foreach($a['value'] as $val)
			{
				$string .= "<input type=hidden name=\"$a[table_name][$a[column_name]][]\" id=\"$a[id]_$x value=\"".formmod($val)."\" $a[additional_params]>";
				$x++;
			}
		}
		else
			$string = "<input type=\"hidden\" name=\"$a[table_name][$a[column_name]]\" id=\"$a[id]\" value=\"".formmod($a['value'])."\" $a[additional_params]>";
		return $string;
	}
	else
	{
	        return "<span style=\"color:red\">Invalid element type</span>";
	}
}

function imageResize($abs_path, $web_path, $size, $target_h, $target_w, $alt, $align, $debug)
{
        if ($debug == '1')
        {
                echo "abs_path: $abs_path <br>\nweb_path: $web_path <br>\nsize: $size <br>\n";
                echo "target_h: $target_h <br>\ntarget_w: $target_w <br>\n";
        }
        // get the height and width of the picture
        $mysock = getimagesize($abs_path);
        $width = $mysock[0];
        $height = $mysock[1];
        if ($debug == '1')
        {  echo "height: $height <br>\nwidth: $width <br>\n"; }
        // if we are finding the target height
        if ($height > $target_h)
        {
                $x = $height / $target_h;
                $width = round($width / $x);
                $height = round($height / $x);
        }
        if ($debug == '1')
        { echo "After height shrink, new height: $height, new width: $width <br>\n"; }
        // if we are finding the target width
        if ($width > $target_w)
        {
                $x = $width / $target_w;
                $width = round($width / $x);
                $height = round($height / $x);
        }
        if ($debug == '1')
        { echo "After width shrink, new height: $height, new width: $width <br>\n"; }
        if ($size && $size < 100)
        {
                $width = $width * ($size / 100);
                $height = $height * ($size / 100);
        }
        if ($debug == '1')
        { echo "After SIZE shrink, new height: $height, new width: $width <br>\n"; }

        $string = "<img src=\"$web_path\" width=\"$width\" height=\"$height\" alt=\"$alt\" align=\"$align\">";
        if ($debug == '1')
        { echo "Final String:<br>\n $string <br>\n"; }
        return $string;
}

