<?php
echo "\n\n-------------------------------------------------------\n\n";
require_once('pg_functions.php');
require_once('pg_db_conn.php');
require_once('pg_users.php');

$pg_connect_string = '';
foreach (array("host", "port", "options") as $var)
    if (isset($db_conn[$var]) && strlen(trim($db_conn[$var])) > 0) $pg_connect_string .= " $var={$db_conn[$var]} ";
foreach (array("dbname", "user", "password") as $var)
    if (isset($superuser[$var]) && strlen(trim($superuser[$var])) > 0) $pg_connect_string .= " $var={$superuser[$var]} ";
if (!$dbh = pg_connect("$pg_connect_string"))
{   echo "Failed to connect to postgres using \"$pg_connect_string\". Confirm access in pg_hba.conf\n\n---------------\n\n"; return 0; }

# can't delete users until everything that depends on users has been deleted or changed to new user.  run again at end to delete users
update_users($dbh, $users, $ignore_delete=1);

echo "\n\n-----------\n\n";
$files = glob('pg_dbs/*.php');
foreach ($files as $file) {
    include $file;
    echo "\n\n----\n\n";

    # if there are any issue, just quit
    if (!update_database($dbh, $db))
        continue;
    if ($db['a'] == 'DELETE' || $db['a'] == 'delete' || $db['a'] == 'IGNORE' || $db['a'] == 'ignore')
        continue;

    # Connect to $d['name'] to issue SQL commands.  Switch back to 'template1' when we're done
    pg_close($dbh);
    $pg_connect_string2 = '';
    foreach (array("host", "port", "options") as $var)
        if (isset($db_conn[$var]) && strlen(trim($db_conn[$var])) > 0) $pg_connect_string2 .= " $var={$db_conn[$var]} ";
    $db_owner_password = ''; foreach($users as $u) if ($u['un'] == $db['o']) $db_owner_password = $u['pw'];
    $pg_connect_string2 .= "dbname={$db['n']} user={$db['o']} password=$db_owner_password";
    if (!$dbh = pg_connect("$pg_connect_string2"))
    {   echo "Failed to connect to postgres using \"$pg_connect_string2\". Confirm access in pg_hba.conf\n\n---------------\n\n"; return 0; }
    echo "  Connected to DB '{$db['n']}' successfully.\n";

    if (!update_schemas($dbh, $db, $schemas))
    {
        echo " Error updating schemas\n";
        continue;
    }
    if (!update_tables($dbh, $db, $tables, $table_info))
    {
        echo " Error updating tables\n";
        continue;
    }
    #if (!update_constraints($dbh, $db, $tables, $table_info))
    #{
    #    echo " Error updating constraints\n";
    #    continue;
    #}

}
echo "\n\n-----------\n\n";

# Connect back to 'template1'
pg_close($dbh);
if (!$dbh = pg_connect("$pg_connect_string"))
{   echo "Failed to connect to postgres using \"$pg_connect_string\". Worked once, and now broken?? Confirm access in pg_hba.conf\n\n---------------\n\n"; return 0; }
echo "\n\n";

update_users($dbh, $users, $ignore_delete=0);
?>
