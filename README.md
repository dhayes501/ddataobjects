# ddataobjects


## Getting started

Make sure postgres is configure with pg_hba.conf to require a password. (i.e NOT with method=trust).

```
cp pg_db_conn.php.sample pg_db_conn.php # edit as needed
cp pg_users.php.sample pg_users.php # edit as needed
dbname="myDatabase"
cp pg_dbs/dbname.php.sample pg_dbs/$dbname.php # edit as needed
php pg_run.php

cp ddataobjects.php.sample ddataobjects.php # edit as needed
php ddataobjects.php
```


