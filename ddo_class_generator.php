<?php

function generate_table($lib_path, $db, $schema, $table_name, $tcs_with_pk, $tpka, $namespace = '')
{
	$prepend = '';
	$prepend_dot = '';
	$prepend_dot_q = '';
	if ($schema['n'] != 'public')
	{
		$prepend = "{$schema['n']}__";
		$prepend_dot = "{$schema['n']}.";
		$prepend_dot_q = "\"{$schema['n']}\".";
	}

	if (substr($table_name, 0, 10) == 'generated_')
	{ echo "Table name cannot begin with 'generated_', this is how we define classes\n\n"; return 0; }
	if ($table_name == 'class_functions')
	{ echo "Table name cannot be 'class_functions', this conflicts with our 'class_functions file\n\n"; return 0; }

 // CREATE GENERATED CLASS
	$file = fopen("$lib_path/generated/generated_$prepend{$table_name}.php", 'w');
	if (!$file)
	{ echo "Failed to open output file"; return 0; }


	$string = "<?php\n";
	if (strlen($namespace) > 0) $string .= "namespace $namespace;\n";
	$string .= "

// class_functions contains: is_int_val(\$x), is_double_val(\$x), is_timestamp_val(\$x), is_time_val(\$x), split_date(\$x)
require_once('$lib_path/generated/generated_class_functions.php');
require_once('$lib_path/generated/generatedRoot.php');
require_once('$lib_path/$prepend$table_name.php');


class generated_$prepend{$table_name} extends generatedRoot
{\n";

	fputs($file, $string);
 // VARIABLE DECLARATIONS ////////////////////////////////////////////////

   // WRITE META DATA ABOUT THIS TABLE
	$meta_data = "  // LOAD META DATA ABOUT TABLE
  public \$_schema = '{$schema['n']}';
  public \$_namespace = '$namespace';
  public \$_name = '{$table_name}';
  public \$_prepend = '$prepend';
  public \$_prepend_dot = '$prepend_dot';
  public \$_prepend_dot_q = '$prepend_dot_q';
  public \$_columns = array(\n";
	$x = 0;
	foreach($tcs_with_pk as $c)
	{
		if ($c['nn'] == 't') $nn = 'true'; else $nn = 'false';
        $column_length = ''; if (preg_match('/([0-9]+)/', "{$c['dt']}", $matches)) $column_length = $matches[1];
        $comment = ''; if (strlen("{$c['c']}") > 0) $comment = str_replace('"', '\\"', $c['c']);

		$meta_data .= "      '{$c['n']}' => array( 'name'=>'{$c['n']}', 'type'=>'{$c['dt']}', 'length'=>'$column_length', 'notnull'=>$nn, 'default'=>\"{$c['d']}\", 'comment'=>\"$comment\" ),\n";
        $x++;
	}
    $meta_data .= "    );\n";
	$meta_data .= "  public \$_primary_key_column = '{$tpka['n']}';\n";
	$meta_data .= "  public \$_primary_key_columns = array();\n";
	$meta_data .= "  public \$_primary_key = true;\n";
	$meta_data .= "  public \$_primary_keys = false;\n";

    // WRITE REGULAR TABLE VARIABLES
	fputs($file, $meta_data."\n\n  // LOAD TABLE VARIABLES\n");

	foreach($tcs_with_pk as $c)
	{

		$string = "  public \${$c['n']};\n";
        if (in_array($c['dt'], ['date', 'timestamp', 'time', 'currency'])) $string .= "  public \${$c['n']}_display;\n";
		if ($c['dt'] == 'boolean') $string .= "  public \${$c['n']}_b;\n";
		$string .= "  public \${$c['n']}_label = '".ucfirst(str_replace("_", " ", $c['n']))."';\n";
		if (strlen(trim($c['c'])) > 0) $string .= "  public \${$c['n']}_regular_comments = '".str_replace("'", "\'", $c['c'])."';\n";
		fputs($file, $string);

	}

    ////////////////////////////////////////////////////////////////////////

    $string = "
      // CREATE STEP VARIABLE HOLDERS
      public \$_step_type; // array or class
      public \$_step_resource; // holds the result of the postgres query
      public \$_step_number; // which record we're on
    ";
    fputs($file, $string);


    $string = "
    // Constructor, sets \$this->_dbh - Retrieves $table_name, if specified by \${$tpka['n']}.\n";
$string .= "  public function __construct(\${$tpka['n']} = '', \$set_from_query = null)\n";
$string .= "  {\n";
$string .= "	parent::__construct();";
$string .= "
	if (\${$tpka['n']} != '' && is_int_val(\${$tpka['n']}))
	{
		\$this->set_variable('{$tpka['n']}', \${$tpka['n']});
		\$this->get();
	}
	elseif(\$set_from_query)
	{
		if (\$results = pg_sfetch_all(\"SELECT * FROM ".addslashes($prepend_dot_q)."\\\"$table_name\\\" WHERE \$set_from_query\"))
			if (count(\$results) == 1)
				foreach(\$results[0] as \$k=>\$v)
					if (@method_exists(\$this, \"set_\$k\"))
						\$this->{\"set_\$k\"}(\$v);
	}
  }\n\n";
		fputs($file, $string);
 ////////////////////////////////////////////////////////////////////////


 // SET VARIABLE FUNCTIONS //////////////////////////////////////////////
	fputs($file, "\n  ////////// VARIABLE SETTER FUNCTIONS FOR COLUMNS\n");
	foreach($tcs_with_pk as $c)
		fputs($file, "  public function set_{$c['n']}(\${$c['n']} = '') { return \$this->set_variable('{$c['n']}', \${$c['n']}); }\n");
 ////////////////////////////////////////////////////////////////////////


 // GET VARIABLE FUNCTIONS //////////////////////////////////////////////
	fputs($file, "\n  ////////// VARIABLE GETTER FUNCTIONS FOR COLUMNS\n");
	foreach($tcs_with_pk as $c)
	{
		fputs($file, "  public function get_{$c['n']}() { return \$this->get_variable('{$c['n']}'); }\n");
		fputs($file, "  public function get_form_{$c['n']}() { return \$this->get_form_variable('{$c['n']}'); }\n");
		if ($c['dt'] == 'date')
			fputs($file, "  public function get_form_{$c['n']}_display() { return \$this->get_form_variable_display('{$c['n']}'); }\n");
		if ($c['dt'] == 'boolean')
			fputs($file, "  public function toggle_{$c['n']}() { return \$this->toggle('{$c['n']}'); }\n");
		fputs($file, "  public function get_{$c['n']}_ie(\$type='', \$value='', \$size_rows_elements='', \$columns_spacer_multiple='', \$display=FALSE, \$additional_params='', \$use_default_value='', \$alt_css_id='')
  { return \$this->get_variable_ie('{$c['n']}', \$type, \$value, \$size_rows_elements, \$columns_spacer_multiple, \$display, \$additional_params, \$use_default_value, \$alt_css_id); }\n");
	}
 ////////////////////////////////////////////////////////////////////////


	fputs($file, "}?>");
	fclose($file);

 // CREATE GENERIC CLASS ////////////////////////////////////////////////
if (is_file("$lib_path/$prepend$table_name.php"))
{
	$file = fopen("$lib_path/$prepend$table_name.php", 'r');
	$data = trim(fread($file, filesize("$lib_path/$prepend$table_name.php")));
	fclose($file);
	if (strlen($data) > 0)
	{
	        $lines = explode("\n", $data);
	        $begin = 0;
	        $ending = '';
	        foreach($lines as $line)
	        {
	                if ($begin)
	                        $ending .= "$line\n";

	                if (trim($line) == '////////////////// All your code below this line ///////////////////////////////')
	                        $begin = 1;
	        }
	}
}
else
        $ending = "}\n\n\n}?>";



$file = fopen("$lib_path/$prepend$table_name.php", 'w');
$string = "<?php\n";
if (strlen($namespace) > 0) $string .= "namespace $namespace;\n";
$string .= "
require_once('$lib_path/generated/generated_$prepend$table_name.php');

class $prepend$table_name extends generated_$prepend$table_name
{
  /**
  * Retrieves $prepend$table_name";
if ($tpka['n']) $string .= ", if specified by \${$tpka['n']}"; $string .= ". Calls global database object
  *
  * @param int \${$tpka['n']}, ID of $table_name to get
  * @global object, \$_dbh database access object
  * @access protected
  */\n";
$string .= "  public function __construct(";
if (trim($tpka['n']) != '')
$string .= "\${$tpka['n']} = '', ";
$string .= "\$set_from_query = null)
  {\n";
$string .= "        parent::__construct(";
if (trim($tpka['n']) != '')
$string .= "\${$tpka['n']}, ";
$string .= "\$set_from_query);
////////////////// All your code below this line ///////////////////////////////
$ending";
fputs($file, $string);
fclose($file);
 ////////////////////////////////////////////////////////////////////////
	return true;
}
?>
