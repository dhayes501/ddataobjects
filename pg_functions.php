<?php

function log_sql($string, $filename = 'pg_cmds.log')
{
    $string = preg_replace("/PASSWORD '.*'/", "PASSWORD '********'", $string);
    $file = fopen($filename, 'a');
    if ($file === false) { echo "Error opening file: $filename"; return; }
    $timestamp = date("Y-m-d H:i:s");
    fwrite($file, "[$timestamp] $string\n");
    fclose($file);
}

function get_current_users($dbh)
{
    $users = array();
    $query = "SELECT usename FROM pg_user";
    #log_sql($query);
    $result = pg_query($dbh, $query);
    if ($result) { while ($row = pg_fetch_assoc($result)) { $users[] = $row; } }
    else { echo "Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
    return $users;
}

function check_current_user_password($user, $password, $db)
{
    if (!isset($password) || strlen(trim($password)) == 0)
        return true;
    require_once('pg_db_conn.php');
    $pg_connect_string = '';
    foreach (array("host", "port", "options") as $var)
        if (isset($db_conn[$var]) && strlen(trim($db_conn[$var])) > 0) $pg_connect_string .= " $var={$db_conn[$var]} ";
    $pg_connect_string .= " dbname=$db user=$user password=$password ";
    $test_connection = @pg_connect("$pg_connect_string");
    if (!$test_connection)
    { return false; }
    pg_close($test_connection);
    return true;
}

function update_users($dbh, $users, $ignore_delete)
{
    echo "Update Users:\n";
    if (!$current_users = get_current_users($dbh))
    { echo "Failed to get current users.\n"; return 0; }
    foreach($users as $u)
    {
        echo "  User: {$u['un']}\n";
        $exists = false;
        foreach($current_users as $cu)
        { if ($cu['usename'] == $u['un']) { $exists = true; break; }}
        if ($u['a'] == 'ignore' || $u['a'] == 'IGNORE')
        { echo "    - Ignore User.\n"; continue; }
        if ($u['a'] == 'delete' || $u['a'] == 'DELETE')
        {
            if (!$exists)
            { echo "    - User does not exist. Can't delete.\n"; continue; }
            if ($ignore_delete)
            { echo "    - Ignoring delete.\n"; continue; }
            $query = "DROP USER {$u['un']}";
            log_sql($query);
            $result = pg_query($dbh, $query);
            if ($result) { echo "    - User has been deleted successfully.\n"; }
                else { echo "Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            continue;
        }
        # see if user exists.  if does not exist, create.  if exists, update password and permissions
        if (!$exists)
        {
            $createdb = 'NOCREATEDB'; if ($u['createdb'] == 't') $createdb = 'CREATEDB';
            $query = "CREATE USER {$u['un']} WITH PASSWORD '{$u['pw']}' $createdb";
            log_sql($query);
            $result = pg_query($dbh, $query);
            if ($result) { echo "    - User created successfully\n"; }
                else { echo "Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
        }
        else
        {
            # only process password if its set
            if (isset($u['pw']) && strlen(trim($u['pw'])) > 0)
            {
                $pw_check = check_current_user_password($u['un'], $u['pw'], 'template1');
                if (!$pw_check)
                {
                    $query = "ALTER USER {$u['un']} WITH PASSWORD '{$u['pw']}'";
                    log_sql($query); # these are superfluous
                    $result = pg_query($dbh, $query);
                    if ($result) { echo "    - Password updated\n"; }
                        else { echo "Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
                }
            }
            $query = "SELECT rolname, rolcreatedb FROM pg_roles WHERE rolname = '{$u['un']}'";
            if (!$result = pg_query($dbh, $query))
            { echo "Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            $row = pg_fetch_assoc($result);
            if ($row['rolcreatedb'] == 't' && $u['createdb'] != 't' || $row['rolcreatedb'] == 'f' && $u['createdb'] != 'f')
            {
                $createdb = 'NOCREATEDB'; if ($u['createdb'] == 't') $createdb = 'CREATEDB';
                $query = "ALTER USER {$u['un']} $createdb";
                log_sql($query); # these are superfluous
                $result = pg_query($dbh, $query);
                if ($result) { echo "    - Permissions updated: ".(($u['createdb'] == 't') ? "CREATEDB" : "NOCREATEDB")."\n"; }
                    else { echo "Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            }
        }
    }
}

function get_current_databases($dbh)
{
    $databases = array();
    $query = "SELECT datname FROM pg_database";
    #log_sql($query);
    $result = pg_query($dbh, $query);
    if ($result) { while ($row = pg_fetch_assoc($result)) { $databases[] = $row; } }
    else { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
    return $databases;
}

function update_database($dbh, $d)
{
    echo "  Database '{$d['n']}':\n";
    if (!$current_users = get_current_users($dbh))
    { echo "  Failed to get current users.\n"; return 0; }
    if (!$current_databases = get_current_databases($dbh))
    { echo "  Failed to get current databases.\n"; return 0; }
    if ($d['a'] == 'ignore' || $d['a'] == 'IGNORE')
    { echo "    - Ignore DB.\n"; return 0; }
    $exists = false;
    foreach($current_databases as $cd)
    { if ($cd['datname'] == $d['n']) { $exists = true; break; }}
    if ($d['a'] == 'delete' || $d['a'] == 'DELETE')
    {
        if (!$exists)
        { echo "    - DB does not exist. Can't delete.\n"; return 0; }
        $query = "DROP DATABASE {$d['n']}";
        log_sql($query);
        $result = pg_query($dbh, $query);
        if ($result) { echo "    - Database '{$d['n']}' has been deleted successfully.\n"; }
            else { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
        return 0;
    }
    $user_exists = false;
    foreach($current_users as $cu)
    { if ($cu['usename'] == $d['o']) { $user_exists = true; break; }}
    if (!$user_exists)
    { echo "    ERROR: Owner is set to '{$d['o']}', but there is no postgres user '{$d['o']}'\n"; return 0; }
    if (!$exists)
    {
        $query = "CREATE DATABASE {$d['n']} WITH OWNER = {$d['o']}";
        log_sql($query);
        $result = pg_query($dbh, $query);
        if ($result) { echo "    - Database created successfully\n"; }
            else { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
    }
    else
    {
        $query = "SELECT r.rolname AS owner FROM pg_database d JOIN pg_roles r ON d.datdba = r.oid WHERE d.datname = '{$d['n']}';";
        #log_sql($query);
        $result = pg_query($dbh, $query);
        if (!$result)
        { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
        $result_row = pg_fetch_row($result);
        if ($result_row[0] != $d['o'])
        {
            $query = "ALTER DATABASE {$d['n']} OWNER TO {$d['o']}";
            log_sql($query);
            $result = pg_query($dbh, $query);
            if ($result) { echo "    - Owner updated\n"; }
                else { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
        }
    }
    return 1;
}

function get_current_schemas($dbh, $d)
{
    $schemas = array();
    $query = "SELECT schema_name, schema_owner FROM information_schema.schemata WHERE catalog_name = '{$d['n']}';";
    #log_sql($query);
    $result = pg_query($dbh, $query);
    if ($result) { while ($row = pg_fetch_assoc($result)) { $schemas[] = $row; } }
    else { echo "    Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
    return $schemas;
}

function update_schemas($dbh, $d, $schemas)
{
    echo "    Update Schemas:\n";
    if (!$current_users = get_current_users($dbh))
    { echo "    Failed to get current users.\n"; return 0; }
    if (!$current_schemas = get_current_schemas($dbh, $d))
    { echo "    Failed to get current schemas.\n"; return 0; }
    foreach($schemas as $s)
    {
        echo "    - Schema: {$s['n']}\n";
        if ($s['a'] == 'ignore' || $s['a'] == 'IGNORE')
        { echo "      - Ignore Schema.\n"; return 0; }
        $exists = false;
        foreach($current_schemas as $cs)
        { if ($cs['schema_name'] == $s['n']) { $exists = true; break; }}
        if ($s['a'] == 'delete' || $s['a'] == 'DELETE')
        {
            if (!$exists)
            { echo "      - Schema does not exist. Can't delete.\n"; return 0; }
            $query = "DROP SCHEMA \"{$s['n']}\"";
            log_sql($query);
            $result = pg_query($dbh, $query);
            if ($result) { echo "      - Schema '{$s['n']}' has been deleted successfully.\n"; }
            else { echo "    Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            return 0;
        }
        if ($s['o'] == 'db_owner') { $s['o'] = $d['o']; }
        $user_exists = false;
        foreach($current_users as $cu)
        { if ($cu['usename'] == $s['o']) { $user_exists = true; break; }}
        if (!$user_exists)
        { echo "      ERROR: Owner is set to '{$s['o']}', but there is no postgres user '{$s['o']}'\n"; return 0; }
        if (!$exists)
        {
            $query = "CREATE SCHEMA \"{$s['n']}\" AUTHORIZATION {$s['o']}";
            log_sql($query);
            $result = pg_query($dbh, $query);
            if ($result) { echo "      - Schema created successfully\n"; }
            else { echo "    Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
        }
        else
        {
            $query = "SELECT rolname AS owner FROM pg_namespace JOIN pg_roles ON nspowner = pg_roles.oid WHERE nspname = '{$s['n']}'; ";
            #log_sql($query);
            $result = pg_query($dbh, $query);
            if (!$result)
            { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            $result_row = pg_fetch_row($result);
            if ($result_row[0] != $s['o'])
            {
                $query = "ALTER schema \"{$s['n']}\" OWNER TO {$s['o']}";
                log_sql($query);
                $result = pg_query($dbh, $query);
                if ($result) { echo "    - Owner updated\n"; }
                    else { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            }
        }
    }
    return 1;
}

function get_current_tables($dbh, $d)
{
    $tables = array();
    $query = "SELECT table_name, table_schema FROM information_schema.tables WHERE table_type = 'BASE TABLE';";
    #log_sql($query);
    $result = pg_query($dbh, $query);
    if ($result) { while ($row = pg_fetch_assoc($result)) { $tables[] = $row; } }
    else { echo "\t  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
    return $tables;
}

function update_tables($dbh, $d, $tables, $table_info)
{
    echo "\t  Update Tables:\n";
    if (!$current_users = get_current_users($dbh))
    { echo "\t  Failed to get current users.\n"; return 0; }
    if (!$current_schemas = get_current_schemas($dbh, $d))
    { echo "\t  Failed to get current schemas.\n"; return 0; }
    if (!$current_tables = get_current_tables($dbh, $d))
    { echo "\t  Failed to get current tables.\n"; return 0; }
    foreach($tables as $t)
    {
        echo "\t  - Table: {$t['s']}.{$t['n']}\n";
        if ($t['a'] == 'ignore' || $t['a'] == 'IGNORE')
        { echo "\t\t- Ignore Table.\n"; return 0; }
        if (strpos($t['n'], "generated_") === 0)
        { echo "\t\t- Table '{$t['n']}' starts with 'generated_'. ddataobjects uses this format internally.  defining tables like this would break functionality and isn't supported. Skipping\n"; return 0; }
        $exists = false;
        foreach($current_tables as $ct)
        { if ($ct['table_name'] == $t['n'] && $ct['table_schema'] == $t['s']) { $exists = true; break; } }
        if ($t['a'] == 'delete' || $t['a'] == 'DELETE')
        {
            if (!$exists)
            { echo "\t\t- Table does not exist. Can't delete.\n"; return 0; }
            $query = "DROP TABLE IF EXISTS \"{$t['s']}\".\"{$t['n']}\"";
            log_sql($query);
            $result = pg_query($dbh, $query);
            if ($result) { echo "\t\t- Table '{$t['s']}'.'{$t['n']}' has been deleted successfully.\n"; }
            else { echo "\t  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            return 0;
        }
        if ($t['o'] == 'db_owner') { $t['o'] = $d['o']; }
        if ($t['o'] == 'schema_owner')
        {
            foreach($current_schemas as $cs)
            { if ($cs['schema_name'] == $t['s']) { $t['o'] = $cs['schema_owner']; } }
            if ($t['o'] == 'schema_owner')
                { echo "\t  Failed to find schema owner for table '{$t['s']}'.'{$t['n']}'"; return 0; }
        }
        $user_exists = false;
        foreach($current_users as $cu)
        { if ($cu['usename'] == $t['o']) { $user_exists = true; break; }}
        if (!$user_exists)
        { echo "\t\tERROR: Owner is set to '{$t['o']}', but there is no postgres user '{$t['o']}'\n"; return 0; }
        if (!$exists)
        {
            if (strlen(trim($t['in'])) == 0) $t['in'] = "${t['n']}_id";
            if (strlen(trim($t['it'])) == 0) $t['it'] = "SERIAL";
            $query = "CREATE TABLE \"{$t['s']}\".\"{$t['n']}\" ({$t['in']} {$t['it']} PRIMARY KEY";
            if (isset($table_info["{$t['s']}"]["{$t['n']}"]['columns']))
            {
                foreach($table_info["{$t['s']}"]["{$t['n']}"]['columns'] as $tc)
                {
                    if ($tc['a'] == 'ignore' || $tc['a'] == 'IGNORE' || $tc['a'] == 'delete' || $tc['a'] == 'DELETE')
                        continue;

                    $isFunction = preg_match('/^\w+\(.*\)$/', $tc['d']);
                    if ($isFunction)
                        $tc_dv = " DEFAULT {$tc['d']}";
                    elseif (strlen(trim($tc['d'])) == 0)
                        $tc_dv = "";
                    else
                        $tc_dv = " DEFAULT '{$tc['d']}'";

                    $query .= ", \"{$tc['n']}\" {$tc['dt']}".(($tc['nn'] == 't') ? " NOT NULL" : "")."$tc_dv";
                }
            }
            $query .= ")";
            log_sql($query);
            $result = pg_query($dbh, $query);
            if ($result) { echo "\t\t- Table created successfully\n"; }
            else { echo "\t  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
        }
        if (isset($table_info["{$t['s']}"]["{$t['n']}"]['columns']))
        {
            if (!update_table_columns($dbh, $d, $t, $table_info["{$t['s']}"]["{$t['n']}"]['columns']))
            { echo "  Failed to get current table columns.\n"; return 0; }
        }

        $query = "SELECT pg_tables.tableowner AS owner FROM pg_tables WHERE pg_tables.schemaname = '{$t['s']}' AND pg_tables.tablename = '{$t['n']}'";
        #log_sql($query);
        $result = pg_query($dbh, $query);
        if (!$result)
        { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
        $result_row = pg_fetch_row($result);
        if ($result_row[0] != $t['o'])
        {
            $query = "ALTER TABLE \"{$t['s']}\".\"{$t['n']}\" OWNER TO {$t['o']}";
            log_sql($query);
            $result = pg_query($dbh, $query);
            if ($result) { echo "    - Owner updated\n"; }
                else { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
        }

    }
    return 1;
}

function get_current_table_columns($dbh, $d, $t)
{
    $columns = array();
#SELECT * FROM information_schema.columns;
# table_catalog: $database_name
# table_schema: $schema_name
# table_name: $table_name
# column_name: string
# ordinal_position: 1,2,3 (order columns were created)
# column_default: e.g. nextval('user_user_id_seq'::regclass)
# is_nullable: YES or NO
# data_type: integer, character varying, timestamp without time zone, numeric, boolean, etc
# character_maximum_length: e.g. '50' (for character varying)
# character_octet_length: don't need, impacted by encoding (UTF8)
# numeric_precision: 32 (for integer), for 'numeric', this will be the total number of digits allowed (both before and after the decimal)
# numeric_precision_radix: 2,10,etc (2 is binary, 10 is base10)
# numeric_scale: for 'numeric' data types, this is how many places before/after the decimal (2 = 2 decimal places, -2 will round to 100's.)
# datetime_precision: for columns with time (datetime or timestamp), this is the fractional seconds precision - defaults to 6?
#### interval_type: null # interval_precision: null # character_set_catalog: null # character_set_schema: null # character_set_name: null # collation_catalog : null # collation_schema: null # collation_name: null
#### domain_catalog: null # domain_schema: null # domain_name: null
# udt_catalog: $database_name
# udt_schema: pg_catalog
# udt_name: int4, varchar, timestamp, numeric, bool
#### scope_catalog: null # scope_schema: null # scope_name: null # maximum_cardinality: null
# dtd_identifier: 1,2,3 (order columns were created)  - ChatGPT thought this might contain full type description. e.g. "integer", "character varying(50)", "numeric(10,2)", etc. but mine doesn't.
# is_self_referencing: NO
# is_identity: NO
#### identity_generation: null # identity_start: null # identity_increment: null # identity_maximum: null # identity_minimum: null # identity_cycle: NO # is_generated: NEVER # generation_expression: null
# is_updatable: YES
    $columns_to_query = 'column_name, column_default, is_nullable, data_type, character_maximum_length, numeric_precision, numeric_precision_radix, numeric_scale, datetime_precision, udt_name, is_updatable';
    $query = "SELECT $columns_to_query FROM information_schema.columns WHERE table_schema = '{$t['s']}' AND table_name = '{$t['n']}';";
    #log_sql($query);
    $result = pg_query($dbh, $query);
    if ($result) { while ($row = pg_fetch_assoc($result)) { $columns[] = $row; } }
    else { echo "\t\t  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
    return $columns;
}

function update_table_columns($dbh, $d, $t, $tcs)
{
    echo "\t\tUpdate Table Columns:\n";
    if (!$current_table_columns = get_current_table_columns($dbh, $d, $t))
    { echo "\t\t  Failed to get current table columns.\n"; return 0; }
    foreach($tcs as $tc)
    {
        echo "\t\t  - Table Column: {$tc['n']}\n";
        if ($tc['a'] == 'ignore' || $tc['a'] == 'IGNORE')
        { echo "\t\t  - Ignore Table Column.\n"; continue; }
        $exists = false;
        foreach($current_table_columns as $ctc)
        { if ($ctc['column_name'] == $tc['n']) { $exists = true; break; } }
        $rename = false;
        foreach($current_table_columns as $ctc)
        { if ($ctc['column_name'] == $tc['on']) { $rename = true; break; } }
        if ($tc['a'] == 'delete' || $tc['a'] == 'DELETE')
        {
            if (!$exists)
            { echo "\t\t  - Table column does not exist. Can't delete.\n"; continue; }
            $query = "ALTER TABLE \"{$t['s']}\".\"{$t['n']}\" DROP COLUMN \"{$tc['n']}\"";
            log_sql($query);
            $result = pg_query($dbh, $query);
            if ($result) { echo "\t\t  - Table Column '{$tc['n']}' has been deleted successfully.\n"; }
            else { echo "\t\t  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            continue;
        }
        if (!$exists && !$rename)
        {
            $isFunction = preg_match('/^\w+\(.*\)$/', $tc['d']);
            if ($isFunction)
                $tc_dv = " DEFAULT {$tc['d']}";
            elseif (strlen(trim($tc['d'])) == 0)
                $tc_dv = "";
            else
                $tc_dv = " DEFAULT '{$tc['d']}'";

            $query = "ALTER TABLE \"{$t['s']}\".\"{$t['n']}\" ADD COLUMN \"{$tc['n']}\" {$tc['dt']}".(($tc['nn'] == 't') ? " NOT NULL" : "")."$tc_dv";
            log_sql($query);
            $result = pg_query($dbh, $query);
            if ($result) { echo "\t\t\t Table column created successfully\n"; }
            else { echo "\t\t  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
        }
        else
        {
            # See if we need to rename the column
            if ($rename)
            {
                $current_table_name = $tc['on'];
                $new_table_name = $tc['n'];
                $current = array(); foreach($current_table_columns as $ctc) if ($ctc['column_name'] == $current_table_name) $current = $ctc;
                if ($current['column_name'] != $current_table_name)
                { echo "\t\t  Failed to find existing column '{$tc['on']}"; }
                else
                {
                    if (strlen(trim($new_table_name)) > 0)
                    {
                        $query = "ALTER TABLE \"{$t['s']}\".\"{$t['n']}\" RENAME COLUMN \"$current_table_name\" TO \"$new_table_name\"";
                        log_sql($query);
                        $result = pg_query($dbh, $query);
                        if ($result) { echo "\t\t  - Field name updated\n"; }
                        else { echo "\t\t  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }

                    }
                }
            }

            # See if we need to attempt to change the column type
            $query = "SELECT column_name, data_type, character_maximum_length, numeric_precision, numeric_scale FROM information_schema.columns WHERE table_schema = '{$t['s']}' AND table_name = '{$t['n']}' AND column_name = '{$tc['n']}'";
            #log_sql($query);
            $result = pg_query($dbh, $query);
            if (!$result)
            { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            $result_all = pg_fetch_all($result);
            $current_column_name = $result_all[0]['column_name'];
            $current_data_type = $result_all[0]['data_type'];
            $current_character_maximum_length = (int) $result_all[0]['character_maximum_length'];
            $current_numeric_precision = (int) $result_all[0]['numeric_precision'];
            $current_numeric_scale = (int) $result_all[0]['numeric_scale'];
            if ($current_character_maximum_length > 0) $current_data_type = "$current_data_type($current_character_maximum_length)";
            if ($current_numeric_precision > 0 && $current_numeric_scale > 0) $current_data_type = "$current_data_type($current_numeric_precision,$current_numeric_scale)";

            if ($current_data_type != $tc['dt'])
            {
                $query = "ALTER TABLE \"{$t['s']}\".\"{$t['n']}\" ALTER COLUMN \"{$tc['n']}\" TYPE {$tc['dt']}";
                log_sql($query);
                $result = pg_query($dbh, $query);
                if ($result) { echo "    - Data Type updated\n"; }
                else { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            }

            # See if we need to attempt to toggle NOT NULL
            $query = "SELECT is_nullable FROM information_schema.columns WHERE table_schema = '{$t['s']}' AND table_name = '{$t['n']}' AND column_name = '{$tc['n']}'";
            #log_sql($query);
            $result = pg_query($dbh, $query);
            if (!$result)
            { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            $result_row = pg_fetch_row($result);
            $alter_nn = '';
            if ($result_row[0] == 'YES' && $tc['nn'] == 't')
                $alter_nn = 'SET';
            if ($result_row[0] == 'NO' && $tc['nn'] == 'f')
                $alter_nn = 'DROP';
            if ($alter_nn != '')
            {
                $query = "ALTER TABLE \"{$t['s']}\".\"{$t['n']}\" ALTER COLUMN \"{$tc['n']}\" $alter_nn NOT NULL";
                log_sql($query);
                $result = pg_query($dbh, $query);
                if ($result) { echo "    - Not Null $alter_nn updated\n"; }
                    else { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            }

            # See if we need to update the Default Value
            $query = "SELECT column_default AS current_default_value FROM information_schema.columns WHERE table_schema = '{$t['s']}' AND table_name = '{$t['n']}' AND column_name = '{$tc['n']}';";
            #log_sql($query);
            $result = pg_query($dbh, $query);
            if (!$result)
            { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            $result_row = pg_fetch_row($result);
            $current_default_value = $result_row[0];
            if ($current_default_value != $tc['d'])
            {
                $isFunction = preg_match('/^\w+\(.*\)$/', $tc['d']);
                if ($isFunction)
                    $tc_dv = "SET DEFAULT {$tc['d']}";
                elseif (strlen(trim($tc['d'])) == 0)
                    $tc_dv = "DROP DEFAULT";
                else
                    $tc_dv = "SET DEFAULT '{$tc['d']}'";

                $query = "ALTER TABLE \"{$t['s']}\".\"{$t['n']}\" ALTER COLUMN \"{$tc['n']}\" $tc_dv";
                log_sql($query);
                $result = pg_query($dbh, $query);
                if ($result) { echo "    - Default Value updated\n"; }
                    else { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            }

            # See if we need to update the Comment
            $query = "SELECT description FROM pg_description WHERE objoid = '{$t['s']}.{$t['n']}'::regclass::oid AND objsubid = ( SELECT attnum FROM pg_attribute WHERE attrelid = '{$t['s']}.{$t['n']}'::regclass AND attname = '{$tc['n']}')";
            #log_sql($query);
            $result = pg_query($dbh, $query);
            if (!$result)
            { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            $result_row = pg_fetch_row($result);
            $current_comment = '';
            if (isset($result_row[0]))
                $current_comment = $result_row[0];
            if ($current_comment != $tc['c'])
            {
                $query = "COMMENT ON COLUMN \"{$t['s']}\".\"{$t['n']}\".\"{$tc['n']}\" IS '{$tc['c']}'";
                log_sql($query);
                $result = pg_query($dbh, $query);
                if ($result) { echo "    - Comment updated\n"; }
                    else { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
            }


        }
    }
    return 1;
}

function get_current_constraints($dbh, $t)
{
    $constraints = array();
    $query = "SELECT tc.constraint_name, tc.constraint_type, kcu.column_name, ccu.table_name AS referenced_table, ccu.column_name AS referenced_column FROM information_schema.table_constraints tc ";
    $query .= "JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name ";
    $query .= "JOIN information_schema.constraint_column_usage ccu ON tc.constraint_name = ccu.constraint_name ";
    $query .= "WHERE tc.table_name = '{$t['n']}';";
    #log_sql($query);
    $result = pg_query($dbh, $query);
    if ($result) { while ($row = pg_fetch_assoc($result)) { $constraints[] = $row; } }
    else { echo "\t  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
    return $constraints;
}

function update_constraints($dbh, $d, $tables, $table_info)
{
    echo "\t  Update Tables Constraints:\n";
    if (!$current_tables = get_current_tables($dbh, $d))
    { echo "\t  Failed to get current tables.\n"; return 0; }

    foreach($tables as $t)
    {
        if (!$current_constraints = get_current_constraints($dbh, $t))
        { echo "\t  Failed to get current constraints.\n"; return 0; }
        echo "\t  - Table: {$t['s']}.{$t['n']}\n";
        if ($t['a'] == 'ignore' || $t['a'] == 'IGNORE')
        { echo "\t\t- Ignore Table.\n"; return 0; }
        $table_exists = false;
        foreach($current_tables as $ct)
        { if ($ct['table_name'] == $t['n'] && $ct['table_schema'] == $t['s']) { $table_exists = true; break; } }
        if (!$table_exists)
        { echo "\t  Table appears to not exist.\n"; return 0; }

        if (!isset($table_info[$t['s']][$t['n']]['constraints']) || !is_array($table_info[$t['s']][$t['n']]['constraints']) || count($table_info[$t['s']][$t['n']]['constraints']) == 0)
        { echo "\t  Table does not have constraints set.\n"; return 1; }

        foreach($table_info[$t['s']][$t['n']]['constraints'] as $constraint)
        {
            $constraint_table = $t['n'];
            if ($constraint['t'] == 'u') { $constraint_type = "UNIQUE"; $constraint_name_type = "key"; }
            elseif ($constraint['t'] == 'fk') { $constraint_type = "FOREIGN KEY"; $constraint_name_type = "fkey"; }
            else
            { echo "\t  Constraint Type: '{$constraint['t']}' is not supported.\n"; continue; }
            $constraint_column = str_replace(" ", "", $constraint['cl']);
            $constraint_fk_table = $constraint['fkt'];
            $constraint_fk_column = $constraint['fkcl'];
            $constraint_action = $constraint['a'];
            $constraint_name = preg_replace("/[^A-Za-z0-9]/", "_", $constraint_table)."_".preg_replace("/[^A-Za-z0-9]/", "_", $constraint_column)."_$name_type";

            if ($constraint_action == 'ignore' || $constraint_action == 'IGNORE')
            { echo "\t  Ignoring Constraint '$constraint_name'\n"; continue; }

            echo "Constraint Name: $constraint_name\n";

            # First, delete any Foreign Keys or Unique constraints that aren't defined.

            # Then, add any Foreign Keys or Unique constraints that we have defined that aren't in Postgresql

            foreach($current_constraints as $cc)
            {
                if ($cc['constraint_type'] == $constraint_type && $cc['constraint_name'] == $constraint_name)
                {
                    # This constraint already exists
                    $exists = true;
                    # column name has to be correct if the name is correct because $constraint_name is generated based on column_name, so we don't need to confirm that.
                    if ($constraint_type == 'FOREIGN KEY')
                    {
                        if ($cc['referenced_table'] == $constraint_fk_table && $cc['referenced_column'] == $constraint_fk_column)
                            $correct = true;
                    }
                }
            }

        }
        continue;
        return true;

        if (!$exists)
        {

            if (strlen(trim($t['in'])) == 0) $t['in'] = "${t['n']}_id";
            if (strlen(trim($t['it'])) == 0) $t['it'] = "SERIAL";
            $query = "CREATE TABLE \"{$t['s']}\".\"{$t['n']}\" ({$t['in']} {$t['it']} PRIMARY KEY";
            if (isset($table_info["{$t['s']}"]["{$t['n']}"]['columns']))
            {
                foreach($table_info["{$t['s']}"]["{$t['n']}"]['columns'] as $tc)
                {
                    if ($tc['a'] == 'ignore' || $tc['a'] == 'IGNORE' || $tc['a'] == 'delete' || $tc['a'] == 'DELETE')
                        continue;

                    $isFunction = preg_match('/^\w+\(.*\)$/', $tc['d']);
                    if ($isFunction)
                        $tc_dv = " DEFAULT {$tc['d']}";
                    elseif (strlen(trim($tc['d'])) == 0)
                        $tc_dv = "";
                    else
                        $tc_dv = " DEFAULT '{$tc['d']}'";

var_dump($tc_dv); return 0;

                    $query .= ", \"{$tc['n']}\" {$tc['dt']}".(($tc['nn'] == 't') ? " NOT NULL" : "")."$tc_dv";
                }
            }
            $query .= ")";
            log_sql($query);
            $result = pg_query($dbh, $query);
            if ($result) { echo "\t\t- Table created successfully\n"; }
            else { echo "\t  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
        }
        if (isset($table_info["{$t['s']}"]["{$t['n']}"]['columns']))
        {
            if (!update_table_columns($dbh, $d, $t, $table_info["{$t['s']}"]["{$t['n']}"]['columns']))
            { echo "  Failed to get current table columns.\n"; return 0; }
        }

        $query = "SELECT pg_tables.tableowner AS owner FROM pg_tables WHERE pg_tables.schemaname = '{$t['s']}' AND pg_tables.tablename = '{$t['n']}'";
        #log_sql($query);
        $result = pg_query($dbh, $query);
        if (!$result)
        { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
        $result_row = pg_fetch_row($result);
        if ($result_row[0] != $t['o'])
        {
            $query = "ALTER TABLE \"{$t['s']}\".\"{$t['n']}\" OWNER TO {$t['o']}";
            log_sql($query);
            $result = pg_query($dbh, $query);
            if ($result) { echo "    - Owner updated\n"; }
                else { echo "  Query failed:\n\nQuery: $query\n\nError:  ".pg_last_error($dbh); return 0; }
        }

    }
    return 1;
}

?>
