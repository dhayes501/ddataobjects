<?php
$ddataobjects_version = "L-1.0";
echo "\ndDataObjects VERSION $ddataobjects_version\n";
//error_reporting(0);

require_once('ddo_class_generator.php');
require_once('pg_db_conn.php');
require_once('pg_users.php');

$files = glob('pg_dbs/*.php');
foreach ($files as $file) {
	include $file;
    $db_name = $db['n'];

    $pg_connect_string = '';
    foreach (array("host", "port", "options") as $var)
        if (isset($db_conn[$var]) && strlen(trim($db_conn[$var])) > 0) $pg_connect_string .= " $var={$db_conn[$var]} ";
    $pg_connect_string .= " dbname=$db_name user={$db['o']} ";
    foreach($users as $pg_user)
        if ($pg_user['un'] == $db['o'] && isset($pg_user['pw']) && strlen(trim($pg_user['pw'])) > 0) $pg_connect_string .= " password={$pg_user['pw']} ";

    $GLOBALS['database_connection_string'][$db_name] = $pg_connect_string;

    $lib_path = $setup['lib_path']."/".$db_name;
    $include_file = "$lib_path.php";

    if (!is_dir($lib_path)) if (!mkdir($lib_path, 0755, true)) { echo "Whoops, lib_path \"$lib_path\" doesn't exist and we can't create it\n\n"; return 0; }
    if (!is_dir("$lib_path/generated")) if (!mkdir("$lib_path/generated", 0755, true)) { echo "Whoops, lib_path \"$lib_path/generated\" doesn't appear to be writeable\n\n"; return 0;}

    foreach($schemas as $schema)
    {
        if (isset($debug) && $debug)
        {  echo "Process Schema: {$schema['n']}\n"; }

        foreach($table_info["{$schema['n']}"] as $table_name=>$tit)
        {
            if (isset($debug) && $debug)
            {  echo "  Process Table '$table_name'\n"; }

            $prepend = ''; if ($schema['n'] != 'public') $prepend = "{$schema['n']}__";

            # Lookup primary key field name and type, create "columns" array and append it to $
            foreach($tables as $tt)
            {
                if ($tt['s'] == $schema['n'] && $tt['n'] == $table_name)
                {
                    if (strlen($tt['in']) > 0) $tpkn = $tt['in']; else $tpkn = $tt['n']."_id";
                    if (strtoupper($tt['it']) == 'BIGSERIAL') $tpkt = 'bigint'; elseif (strtoupper($tt['it']) == 'SMALLSERIAL') $tpkt = 'smallint'; else $tpkt  = 'integer';
                }
            }
            # table primary key array:

            $tpka_default_value = "nextval('{$table_name}_{$tpkn}_seq'::regclass)";
            $tpka = array(array( "n"=>"$tpkn", "dt"=>"$tpkt", "nn"=>"t", "d"=>"$tpka_default_value",  "c"=>'' ));

            $tcs_with_pk = array_values(array_merge($tpka, $tit['columns']));

            if (!generate_table($lib_path, $db, $schema, $table_name, $tcs_with_pk, $tpka[0], $setup['namespace']))
            { echo "Whoops, failed to generated class for $prepend{$t['n']}\n\n"; return 0; }
            $files[] = basename("$lib_path/$prepend$table_name.php");
        }
    }

    if (is_file($include_file))
    {
            $file = fopen($include_file, 'r');
            $data = trim(fread($file, filesize($include_file)));
            fclose($file);
            if (strlen($data) > 0)
            {
                    $lines = explode("\n", $data);
                    $begin = 0;
                    $ending = '';
                    foreach($lines as $line)
                    {
                            if ($begin)
                                    $ending .= "$line\n";

                            if (trim($line) == '////////////////// All your code below this line ///////////////////////////////')
                                    $begin = 1;
                    }
            }
    }
    else
    {
        $ending = "\nfunction myAutoloader(\$class_name) { include '$lib_path/'.\$class_name.'.php';}\nspl_autoload_register('myAutoloader');\n\n\n?>";
    }


    if (!$file = fopen($include_file, 'w'))
    { echo "Failed to create includes file"; return 0; }

    $string = "<?php\n";
    if (strlen($setup['namespace']) > 0) $string .= "namespace {$setup['namespace']};\n";
    $string .= "
    \$GLOBALS['database_connection_string'] = \"{$GLOBALS['database_connection_string'][$db_name]}\";
    require_once('$lib_path/generated/generated_class_functions.php');
    \n";
    ////////////////////////////////////
    $string .= "////////////////// All your code below this line ///////////////////////////////
    $ending";
    fputs($file, $string);
    fclose($file);

    # create $lib_path/generated/generated_class_functions.php
    $gcf = file_get_contents("ddo_generated_class_functions.php");
    if (strlen($setup['namespace']) > 0) $gcf = str_replace("<?php", "<?php\nnamespace {$setup['namespace']};", $gcf);
    $file = fopen("$lib_path/generated/generated_class_functions.php", 'w');
    fputs($file, $gcf);
    fclose($file);
    /////////////////////////////////////////////////////

    // create $lib_path/generated/generatedRoot.php
    $generated_root = file_get_contents("ddo_generatedRoot.php");
    $lines = explode("\n", $generated_root);
    // remove first 3 lines "<?php" and comments, we'll correctly start the class here
    array_shift($lines);array_shift($lines);array_shift($lines);
    $file = fopen("$lib_path/generated/generatedRoot.php", 'w');
    $string = "<?php\n";
    if (strlen($setup['namespace']) > 0) $string .= "namespace {$setup['namespace']};";
    $string .= "
require_once('$lib_path/generated/generated_class_functions.php');
class generatedRoot
{
	var \$_dbh;
	// if there are too many records in the database, we might need to save memory by only processing one record at a time, the step variables keep the connection information
	var \$_step_type;
	var \$_step_resource;
	var \$_step_number;
	var \$_last_error; // if a query fails, this will store the value of pg_last_error(\$this->_dbh) or the reason why my program didn't run the query.\n\n";
    $string .= "	function __construct()";
    $string .= "
	{
		\$this->_dbh = pg_connect(\$GLOBALS['database_connection_string']);
		if (@!isset(\$this->_dbh))
			return false;
	}
\n\n";
    fputs($file, $string);
    foreach($lines as $line)
        fputs($file, "$line\n");
    fputs($file, "}\n?>");
    fclose($file);


    // Add functions to class functions
    $file = fopen("$lib_path/generated/generated_class_functions.php", 'a');
    $string = "
function pg_begin()
{
	\$_dbh = pg_connect(\$GLOBALS['database_connection_string']);
	if (!pg_query(\$_dbh, \"BEGIN\"))
		return false;
	return true;
}
function pg_rollback()
{
	\$_dbh = pg_connect(\$GLOBALS['database_connection_string']);
	if (!pg_query(\$_dbh, \"ROLLBACK\"))
		return false;
	return true;
}
function pg_commit()
{
	\$_dbh = pg_connect(\$GLOBALS['database_connection_string']);
	if (!pg_query(\$_dbh, \"COMMIT\"))
		return false;
	return true;
}
function pg_squery(\$sql)
{
	\$_dbh = pg_connect(\$GLOBALS['database_connection_string']);
	return pg_query(\$_dbh, \$sql);
}
function pg_sfetch_all(\$variable)
{
	\$type = gettype(\$variable);
	if (\$type == 'resource')
		\$array = pg_fetch_all(\$variable);
	else
	{
		if (\$result = pg_squery(\$variable))
			\$array = pg_fetch_all(\$result);
	}
	return \$array;
}
function pg_sfetch_row(\$variable)
{
	\$type = gettype(\$variable);
	if (\$type == 'resource')
		\$array = pg_fetch_all(\$variable);
	else
	{
		\$result = pg_squery(\$variable);
		\$array = pg_fetch_all(\$result);
	}
	return \$array[0];
}
function pg_sfetch(\$variable)
{
	\$type = gettype(\$variable);
	if (\$type == 'resource')
		\$array = pg_fetch_all(\$variable);
	else
	{
		\$result = pg_squery(\$variable);
		\$array = pg_fetch_all(\$result);
	}
	if (@is_array(\$array))
	{
		foreach(\$array as \$a)
		{
			if (@is_array(\$a))
				foreach(\$a as \$k=>\$v)
					return \$v;
		}
	}
	return false;
}";
    $string .= "
function load_all()
{";
    foreach($schemas as $schema)
	{
        foreach($table_info["{$schema['n']}"] as $table_name=>$tcs)
		{
            $prepend = ''; if ($schema['n'] != 'public') $prepend = "{$schema['n']}__";
			$string .= "	require_once(\"$lib_path/$prepend$table_name.php\");\n";
		}
	}
    $string .= "}

?>";
    fputs($file, $string);
    fclose($file);


    echo "\n\nAll Done\n\n";
}
?>
